#include <Wire.h>
#include "MultiPing.h"

MultiPing mp;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  mp.updateDistances();

  Serial.print("Ping 1: ");
  Serial.print(mp.getDistance(1));
  Serial.print(" mm      ");
  Serial.print("Ping 2: ");
  Serial.print(mp.getDistance(2));
  Serial.print(" mm      ");
  Serial.print("Ping 3: ");
  Serial.print(mp.getDistance(3));
  Serial.print(" mm      ");
  Serial.print("Ping 4: ");
  Serial.print(mp.getDistance(4));
  Serial.println(" mm");

  delay(50);
}
