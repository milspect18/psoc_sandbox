#include "MultiPing.h"

MultiPing::MultiPing() {
    Wire.begin();
}

void MultiPing::updateDistances(int numSensor) {
    uint8_t i = 0;
    uint8_t numBytes = (numSensor * sizeof(sensorDist[0]));
    Wire.requestFrom((uint8_t)8, numBytes);

    while (Wire.available() && i < numBytes) {
        rawData[i] = Wire.read();
        i++;
    }

    sensorDist[0] = (rawData[0] << 8) | (rawData[1]);
    sensorDist[1] = (rawData[2] << 8) | (rawData[3]);
    sensorDist[2] = (rawData[4] << 8) | (rawData[5]);
    sensorDist[3] = (rawData[6] << 8) | (rawData[7]);
}
uint16_t MultiPing::getDistance(int sensorNum) {
    return sensorDist[sensorNum - 1];
}

