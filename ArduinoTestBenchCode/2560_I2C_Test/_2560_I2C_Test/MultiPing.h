#ifndef     __MULTIPING_H
#define     __MULTIPING_H

#include <Wire.h>

class MultiPing {
    public:
        MultiPing();
        ~MultiPing() {}
        void updateDistances(int numSensor = 4);
        uint16_t getDistance(int sensorNum = 1);
    private:
        uint16_t sensorDist[4] = {0};
        uint8_t rawData[8] = {0};
};


#endif
