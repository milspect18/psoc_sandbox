/* =================================================================================================
 *
 * Copyright KPSG Robotics
 * All Rights Reserved
 *
 * Purpose: This PSoC design is meant to be used to gather distance data
 *          from an array of Ping sensors and provide an I2C interface to
 *          a master device that will be able to read those distance values.
 *
 * Notes: To add to te number of Ping sensors used (currently 4) you will 
 *        define a macro for the MUX/DeMUX value used to pass its signals
 *        (see the TopDesign.cysch file) to the timer and IO.  Then update
 *        the NUM_SENSOR macro value.  Add the distance[n] = getPingDistance(pingMacroNameHere);
 *        line to the main event loop.  Finally add the pins and scale the 
 *        MUX and DeMux appropriately.
 *
 * =================================================================================================
*/
#include "project.h"
#include "Utils.h"

// Macros
#define TRIGGER         0x4     // 0100
#define NUM_SENSORS     4       // Number of ping sensors used
#define MM_SCALE        5.8f    // Divisor to convert counter to mm
#define DEBUG_MODE      1


// Prototypes
uint16 getPingDistance(uint8 pingNum);
void updateI2CBuffer(uint8 *i2cBuffer, uint16 *distBuffer, uint8 numMeasurements);
void printSensorData(uint8 *distance);


                                                                                         ///////////
/////////////////////////////////////////////////////////////////////////////////////////// MAIN  //
////////////////////////////////////////////////////////////////////////////////////////////////////
int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. Required for EzI2C */
    
    // Start the counter used for measuring echo timing
    Echo_Counter_Start();
    Timeout_Timer_Start();
    
    // 16 bit distance reading from sensor
    // 8 bit I2C buffer we will pack distance values into
    // 8 bit index for iterating through sensors
    uint16 distance[NUM_SENSORS] = {0};
    uint8 i2cDataBuffer[NUM_SENSORS*sizeof(distance[0])] = {0};
    uint8 sensorIndex = 0;
    
    // Start the I2C
    I2C_IF_Start();
    // The buffer is made read-only to the I2C master as we control the distance data
    I2C_IF_SetBuffer1(NUM_SENSORS*sizeof(distance[0]), 0, i2cDataBuffer);

    // Start the UART
    PC_Start();

    while (1)
    {
        // Get all of the Ping sensor distances and update the I2C buffers with those values
        for (sensorIndex = 0; sensorIndex < NUM_SENSORS; sensorIndex++) {
            distance[sensorIndex] = getPingDistance(sensorIndex);
        }
        
        updateI2CBuffer(i2cDataBuffer, distance, NUM_SENSORS);
        printSensorData(i2cDataBuffer);
        CyDelay(50);    // This just ensures we don't get a ping lock by triggering to rapidly
    }
}
/* END MAIN() */




                                                                              //////////////////////
//////////////////////////////////////////////////////////////////////////////// getPingDistance  //
////////////////////////////////////////////////////////////////////////////////////////////////////
// Return the value of the specified ping sensor
// The time this call takes is dependent upon the length (timer max val = 65.536 ms)
//
// TODO: Improve timeout feature
/**************************************************************************************************/
uint16 getPingDistance(uint8 pingNum) {
    Ping_Sel_Reg_Write(pingNum | TRIGGER);
    CyDelayUs(10);
    Ping_Sel_Reg_Write(pingNum & ~(TRIGGER));   // Clear of trigger not required but left for safety
    
    // Wait for Echo pulse rising, timeout if it takes too long
    while(!Echo_Status_Read()) {
        if (Timeout_Timer_ReadStatusRegister() & Timeout_Timer_STATUS_TC) {
            return 0xFFFF;  // We timed out so return the max value
        }
    };
    while(Echo_Status_Read()) {};      // Wait for Echo pulse falling
    
    // Give back the distance in mm
    return Echo_Counter_ReadCounter() / MM_SCALE; 
}



                                                                              //////////////////////
//////////////////////////////////////////////////////////////////////////////// updateI2CBuffer  //
////////////////////////////////////////////////////////////////////////////////////////////////////
// Inputs: pointer to i2c primary data buffer
//         pointer to i2c secondary data buffer
//         pointer to distance data buffer
//         number of distance measurements/sensors
//
// To avoid the situation where a read request comes in the middle of updating
// the primary I2C data buffer, we disable the I2C interupts while we update the 
// i2cDataBuffer.
//
// The distance calculations are done with 16 bit
// integers (the spec'd max distance for the sensor is 400 cm)
// with 16 bit counters our max would be 1230cm, so this should 
// be plenty of width.  The I2C interface hands data back one byte
// at a time.  This function packs the 16 bit distance data into
// the i2c buffer as highest byte then lowest byte for each distance.
// The sizeof function is used to set the size of the i2c data buffer
// based on the number of sensors times the number of bytes in
// each distance reading.  This will allow scaling of the distance
// parameter if a different more long range sensor is used, without
// having to change the i2c buffer or packing code.
/**************************************************************************************************/
void updateI2CBuffer(uint8 *i2cBuffer, uint16 *distBuffer, uint8 numMeasurements) {
    uint8 i;
    uint8 busIndex = 0;
    
    I2C_IF_DisableInt();
    
    // Update the primary buffer
    for (i = 0; i < numMeasurements; i++) {
        i2cBuffer[busIndex++] = (uint8)(distBuffer[i] >> 8);    // grab highest byte and increment the index
        i2cBuffer[busIndex++] = (uint8)(distBuffer[i] & 0xFF);  // grab lowest byte and increment the index
    }
    
    I2C_IF_EnableInt();
    
}



                                                                             ///////////////////////
//////////////////////////////////////////////////////////////////////////////// printSensorData  //
////////////////////////////////////////////////////////////////////////////////////////////////////
// Temporary function to print the ping distances via UART for human readability 
/**************************************************************************************************/
void printSensorData(uint8 *distance) {
    char buffer[256] = {0};

    PC_PutString("$1");
    PC_PutString(intToString((distance[0] << 8) | (distance[1]), buffer));
    PC_PutString("$2");
    PC_PutString(intToString((distance[2] << 8) | (distance[3]), buffer));
    PC_PutString("$3");
    PC_PutString(intToString((distance[4] << 8) | (distance[5]), buffer));
    PC_PutString("$4");
    PC_PutString(intToString((distance[6] << 8) | (distance[7]), buffer));
    PC_PutString("E");

    if (DEBUG_MODE) {
        PC_PutCRLF();
    }
}

/* [] END OF FILE */
