/*******************************************************************************
* File Name: Ping_4_Trig.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Ping_4_Trig_ALIASES_H) /* Pins Ping_4_Trig_ALIASES_H */
#define CY_PINS_Ping_4_Trig_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*              Constants        
***************************************/
#define Ping_4_Trig_0			(Ping_4_Trig__0__PC)
#define Ping_4_Trig_0_INTR	((uint16)((uint16)0x0001u << Ping_4_Trig__0__SHIFT))

#define Ping_4_Trig_INTR_ALL	 ((uint16)(Ping_4_Trig_0_INTR))

#endif /* End Pins Ping_4_Trig_ALIASES_H */


/* [] END OF FILE */
