/*******************************************************************************
* File Name: Ping_4_Trig.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Ping_4_Trig_H) /* Pins Ping_4_Trig_H */
#define CY_PINS_Ping_4_Trig_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Ping_4_Trig_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Ping_4_Trig__PORT == 15 && ((Ping_4_Trig__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Ping_4_Trig_Write(uint8 value);
void    Ping_4_Trig_SetDriveMode(uint8 mode);
uint8   Ping_4_Trig_ReadDataReg(void);
uint8   Ping_4_Trig_Read(void);
void    Ping_4_Trig_SetInterruptMode(uint16 position, uint16 mode);
uint8   Ping_4_Trig_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Ping_4_Trig_SetDriveMode() function.
     *  @{
     */
        #define Ping_4_Trig_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Ping_4_Trig_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Ping_4_Trig_DM_RES_UP          PIN_DM_RES_UP
        #define Ping_4_Trig_DM_RES_DWN         PIN_DM_RES_DWN
        #define Ping_4_Trig_DM_OD_LO           PIN_DM_OD_LO
        #define Ping_4_Trig_DM_OD_HI           PIN_DM_OD_HI
        #define Ping_4_Trig_DM_STRONG          PIN_DM_STRONG
        #define Ping_4_Trig_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Ping_4_Trig_MASK               Ping_4_Trig__MASK
#define Ping_4_Trig_SHIFT              Ping_4_Trig__SHIFT
#define Ping_4_Trig_WIDTH              1u

/* Interrupt constants */
#if defined(Ping_4_Trig__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Ping_4_Trig_SetInterruptMode() function.
     *  @{
     */
        #define Ping_4_Trig_INTR_NONE      (uint16)(0x0000u)
        #define Ping_4_Trig_INTR_RISING    (uint16)(0x0001u)
        #define Ping_4_Trig_INTR_FALLING   (uint16)(0x0002u)
        #define Ping_4_Trig_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Ping_4_Trig_INTR_MASK      (0x01u) 
#endif /* (Ping_4_Trig__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Ping_4_Trig_PS                     (* (reg8 *) Ping_4_Trig__PS)
/* Data Register */
#define Ping_4_Trig_DR                     (* (reg8 *) Ping_4_Trig__DR)
/* Port Number */
#define Ping_4_Trig_PRT_NUM                (* (reg8 *) Ping_4_Trig__PRT) 
/* Connect to Analog Globals */                                                  
#define Ping_4_Trig_AG                     (* (reg8 *) Ping_4_Trig__AG)                       
/* Analog MUX bux enable */
#define Ping_4_Trig_AMUX                   (* (reg8 *) Ping_4_Trig__AMUX) 
/* Bidirectional Enable */                                                        
#define Ping_4_Trig_BIE                    (* (reg8 *) Ping_4_Trig__BIE)
/* Bit-mask for Aliased Register Access */
#define Ping_4_Trig_BIT_MASK               (* (reg8 *) Ping_4_Trig__BIT_MASK)
/* Bypass Enable */
#define Ping_4_Trig_BYP                    (* (reg8 *) Ping_4_Trig__BYP)
/* Port wide control signals */                                                   
#define Ping_4_Trig_CTL                    (* (reg8 *) Ping_4_Trig__CTL)
/* Drive Modes */
#define Ping_4_Trig_DM0                    (* (reg8 *) Ping_4_Trig__DM0) 
#define Ping_4_Trig_DM1                    (* (reg8 *) Ping_4_Trig__DM1)
#define Ping_4_Trig_DM2                    (* (reg8 *) Ping_4_Trig__DM2) 
/* Input Buffer Disable Override */
#define Ping_4_Trig_INP_DIS                (* (reg8 *) Ping_4_Trig__INP_DIS)
/* LCD Common or Segment Drive */
#define Ping_4_Trig_LCD_COM_SEG            (* (reg8 *) Ping_4_Trig__LCD_COM_SEG)
/* Enable Segment LCD */
#define Ping_4_Trig_LCD_EN                 (* (reg8 *) Ping_4_Trig__LCD_EN)
/* Slew Rate Control */
#define Ping_4_Trig_SLW                    (* (reg8 *) Ping_4_Trig__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Ping_4_Trig_PRTDSI__CAPS_SEL       (* (reg8 *) Ping_4_Trig__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Ping_4_Trig_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Ping_4_Trig__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Ping_4_Trig_PRTDSI__OE_SEL0        (* (reg8 *) Ping_4_Trig__PRTDSI__OE_SEL0) 
#define Ping_4_Trig_PRTDSI__OE_SEL1        (* (reg8 *) Ping_4_Trig__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Ping_4_Trig_PRTDSI__OUT_SEL0       (* (reg8 *) Ping_4_Trig__PRTDSI__OUT_SEL0) 
#define Ping_4_Trig_PRTDSI__OUT_SEL1       (* (reg8 *) Ping_4_Trig__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Ping_4_Trig_PRTDSI__SYNC_OUT       (* (reg8 *) Ping_4_Trig__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Ping_4_Trig__SIO_CFG)
    #define Ping_4_Trig_SIO_HYST_EN        (* (reg8 *) Ping_4_Trig__SIO_HYST_EN)
    #define Ping_4_Trig_SIO_REG_HIFREQ     (* (reg8 *) Ping_4_Trig__SIO_REG_HIFREQ)
    #define Ping_4_Trig_SIO_CFG            (* (reg8 *) Ping_4_Trig__SIO_CFG)
    #define Ping_4_Trig_SIO_DIFF           (* (reg8 *) Ping_4_Trig__SIO_DIFF)
#endif /* (Ping_4_Trig__SIO_CFG) */

/* Interrupt Registers */
#if defined(Ping_4_Trig__INTSTAT)
    #define Ping_4_Trig_INTSTAT            (* (reg8 *) Ping_4_Trig__INTSTAT)
    #define Ping_4_Trig_SNAP               (* (reg8 *) Ping_4_Trig__SNAP)
    
	#define Ping_4_Trig_0_INTTYPE_REG 		(* (reg8 *) Ping_4_Trig__0__INTTYPE)
#endif /* (Ping_4_Trig__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Ping_4_Trig_H */


/* [] END OF FILE */
