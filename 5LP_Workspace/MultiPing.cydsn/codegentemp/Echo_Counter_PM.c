/*******************************************************************************
* File Name: Echo_Counter_PM.c  
* Version 3.0
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "Echo_Counter.h"

static Echo_Counter_backupStruct Echo_Counter_backup;


/*******************************************************************************
* Function Name: Echo_Counter_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Echo_Counter_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void Echo_Counter_SaveConfig(void) 
{
    #if (!Echo_Counter_UsingFixedFunction)

        Echo_Counter_backup.CounterUdb = Echo_Counter_ReadCounter();

        #if(!Echo_Counter_ControlRegRemoved)
            Echo_Counter_backup.CounterControlRegister = Echo_Counter_ReadControlRegister();
        #endif /* (!Echo_Counter_ControlRegRemoved) */

    #endif /* (!Echo_Counter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Echo_Counter_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Echo_Counter_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void Echo_Counter_RestoreConfig(void) 
{      
    #if (!Echo_Counter_UsingFixedFunction)

       Echo_Counter_WriteCounter(Echo_Counter_backup.CounterUdb);

        #if(!Echo_Counter_ControlRegRemoved)
            Echo_Counter_WriteControlRegister(Echo_Counter_backup.CounterControlRegister);
        #endif /* (!Echo_Counter_ControlRegRemoved) */

    #endif /* (!Echo_Counter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Echo_Counter_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Echo_Counter_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void Echo_Counter_Sleep(void) 
{
    #if(!Echo_Counter_ControlRegRemoved)
        /* Save Counter's enable state */
        if(Echo_Counter_CTRL_ENABLE == (Echo_Counter_CONTROL & Echo_Counter_CTRL_ENABLE))
        {
            /* Counter is enabled */
            Echo_Counter_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            Echo_Counter_backup.CounterEnableState = 0u;
        }
    #else
        Echo_Counter_backup.CounterEnableState = 1u;
        if(Echo_Counter_backup.CounterEnableState != 0u)
        {
            Echo_Counter_backup.CounterEnableState = 0u;
        }
    #endif /* (!Echo_Counter_ControlRegRemoved) */
    
    Echo_Counter_Stop();
    Echo_Counter_SaveConfig();
}


/*******************************************************************************
* Function Name: Echo_Counter_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Echo_Counter_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void Echo_Counter_Wakeup(void) 
{
    Echo_Counter_RestoreConfig();
    #if(!Echo_Counter_ControlRegRemoved)
        if(Echo_Counter_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            Echo_Counter_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!Echo_Counter_ControlRegRemoved) */
    
}


/* [] END OF FILE */
