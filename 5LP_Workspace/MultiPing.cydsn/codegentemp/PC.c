/*******************************************************************************
* File Name: PC.c
* Version 1.50
*
* Description:
*  This file provides the source code to the API for the Software Transmit UART.
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "PC_PVT.h"

#if(PC_PIN_STATIC_MODE == 1u)
    uint32 PC_pinNumber = PC_PIN_NUMBER;
    uint32 PC_pinPortNumber = PC_PIN_PORT_NUMBER;
    #if(CY_PSOC3)
        uint32 pdata PC_pinDrAdress = PC_PIN_DR;
    #else
        uint32 PC_pinDrAdress = PC_PIN_DR;
    #endif /* (CY_PSOC3) */
#else
    uint32 PC_pinNumber;
    uint32 PC_pinPortNumber;
    #if(CY_PSOC3)
        uint32 pdata PC_pinDrAdress;
    #else
        uint32 PC_pinDrAdress;
    #endif /* (CY_PSOC3) */
#endif /* (PC_PIN_STATIC_MODE == 1u) */


#if(PC_PIN_STATIC_MODE == 1u)
    /*******************************************************************************
    * Function Name: PC_Start
    ********************************************************************************
    *
    * Summary:
    *  Empty function. Included for consistency with other
    *  components. This API is not available when PinAssignmentMethod
    *  is set to Dynamic.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void PC_Start(void) 
    {

    }
#else
    /*******************************************************************************
    * Function Name: PC_StartEx
    ********************************************************************************
    *
    * Summary:
    *  Configures the SW Tx UART to use the pin specified
    *  by the parameters. This API is only available when
    *  PinAssignmentMethod is set to Dynamic.
    *
    * Parameters:
    *  port:  Port number for dynamic pin assignment
    *  pin:   Pin number for dynamic pin assignment
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void PC_StartEx(uint8 port, uint8 pin) 
    {
        uint32 portConfigAddr;

        #if (CY_PSOC4)
            uint32 portDataRegAddr;
        #endif /* (CY_PSOC4) */

        if ((pin <= PC_MAX_PIN_NUMBER) && (port <= PC_MAX_PORT_NUMBER))
        {
            #if (!CY_PSOC4)
                portConfigAddr = PC_PORT_CNF_BASE;
                portConfigAddr += ((uint32)port * (PC_MAX_PIN_NUMBER + 1u)) + pin;
                CyPins_SetPinDriveMode(portConfigAddr, CY_PINS_DM_STRONG);
                CyPins_SetPin(portConfigAddr);
                PC_pinDrAdress = portConfigAddr;
            #else
                portConfigAddr = PC_PORT_CNF_BASE + (PC_PORT_CNF_SIZE * port) +
                                                                                PC_PORT_CNF_MODE_OFFSET;
                CY_SYS_PINS_SET_DRIVE_MODE(portConfigAddr, pin, CY_SYS_PINS_DM_STRONG);
                portDataRegAddr = PC_PORT_CNF_BASE + (PC_PORT_CNF_SIZE * port) +
                                                                                PC_PORT_CNF_DR_OFFSET;
                CY_SYS_PINS_SET_PIN(portDataRegAddr, pin);
                PC_pinDrAdress = portDataRegAddr;
            #endif /* (!CY_PSOC4) */
            PC_pinNumber = pin;
            PC_pinPortNumber = port;
        }
    }
#endif /* (PC_PIN_STATIC_MODE == 1u) */


/*******************************************************************************
* Function Name: PC_Stop
********************************************************************************
*
* Summary:
*  Empty function. Included for consistency with other components.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void PC_Stop(void) 
{

}


/*******************************************************************************
* Function Name: PC_PutString
********************************************************************************
*
* Summary:
*  Sends a NULL terminated string via the Tx pin.
*
* Parameters:
*  string: Pointer to the null terminated string to send
*
* Return:
*  None
*
*******************************************************************************/
void PC_PutString(const char8 string[]) 
{
    uint8 stringIndex = 1u;
    char8 current = *string;

    /* Until null is reached, print next character */
    while((char8) '\0' != current)
    {
        PC_PutChar((uint8)current);
        current = string[stringIndex];
        stringIndex++;
    }
}


/*******************************************************************************
* Function Name: PC_PutArray
********************************************************************************
*
* Summary:
*  Sends byteCount bytes from a memory array via the Tx pin.
*
* Parameters:
*  data: Pointer to the memory array
*  byteCount: Number of bytes to be transmitted
*
* Return:
*  None
*
*******************************************************************************/
void PC_PutArray(const uint8 array[], uint32 byteCount) 
{
    uint32 arrayIndex;

    for (arrayIndex = 0u; arrayIndex < byteCount; arrayIndex++)
    {
        PC_PutChar(array[arrayIndex]);
    }
}


/*******************************************************************************
* Function Name: PC_PutHexByte
********************************************************************************
*
* Summary:
*  Sends a byte in Hex representation (two characters, uppercase for A-F) via
*  the Tx pin.
*
* Parameters:
*  TxHexByte: The byte to be converted to ASCII characters and
*             sent via the Tx pin.
*
* Return:
*  None
*
*******************************************************************************/
void PC_PutHexByte(uint8 txHexByte) 
{
    static char8 const CYCODE PC_hex[] = "0123456789ABCDEF";

    PC_PutChar((uint8) PC_hex[txHexByte >> PC_BYTE_UPPER_NIBBLE_SHIFT]);
    PC_PutChar((uint8) PC_hex[txHexByte & PC_BYTE_LOWER_NIBBLE_MASK]);
}


/*******************************************************************************
* Function Name: PC_PutHexInt
********************************************************************************
*
* Summary:
*  Sends a 16-bit unsigned integer in Hex representation (four characters,
*  uppercase for A-F) via the Tx pin.
*
* Parameters:
*  TxHexInt: The uint16 to be converted to ASCII characters and sent via
*            the Tx pin.
*
* Return:
*  None
*
*******************************************************************************/
void PC_PutHexInt(uint16 txHexInt) 
{
    PC_PutHexByte((uint8)(txHexInt >> PC_U16_UPPER_BYTE_SHIFT));
    PC_PutHexByte((uint8)(txHexInt & PC_U16_LOWER_BYTE_MASK));
}


/*******************************************************************************
* Function Name: PC_PutCRLF
********************************************************************************
*
* Summary:
*  Sends a carriage return (0x0D) and a line feed (0x0A) via the Tx pin.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void PC_PutCRLF(void) 
{
    PC_PutChar(0x0Du);
    PC_PutChar(0x0Au);
}


/* [] END OF FILE */
