/* ========================================
 *
 *  Simple utility functions in pure C
 *
 * ========================================
*/
#ifndef __UTILS_H
#define __UTILS_H
    
#include "project.h"

char *intToString(uint32 inNum, char *buffer);
void reverseString(char *inStr, uint32 numChars);
char *floatToString(float inNum, char *buffer);
long map(long x, long in_min, long in_max, long out_min, long out_max);
    
    
#endif

/* [] END OF FILE */
