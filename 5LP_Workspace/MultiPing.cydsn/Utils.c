/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "Utils.h"


// Convert an integer to a C style string
char *intToString(uint32 inNum, char *buffer){
    uint32 i = 0;
    
    while (inNum) {
        buffer[i++] = '0' + inNum % 10;
        inNum /= 10;
    }
    
    buffer[i] = '\0';
    reverseString(buffer, i);
    
    return buffer;
}



// Reverse the order of char's in a C style sting
void reverseString(char *inStr, uint32 numChars) {
    int tail = 0;
    int head = numChars - 1;
    char tempTail = 0;
    
    while (head > tail) {
        tempTail = inStr[tail];
        inStr[tail] = inStr[head];
        inStr[head] = tempTail;
        tail++;
        head--;
    }
}


// Convert a floating point number to a C style string (5 point precision)
char *floatToString(float inNum, char *buffer) {
    uint32 integer = (uint32)inNum;
    uint32 decimal = inNum * 100000 - integer * 100000;
    uint32 i = 0;
    uint8 j = 0;
    char temp[6] = {0};

    while (integer) {
        buffer[i++] = '0' + integer % 10;
        integer /= 10;
    }
    
    reverseString(buffer, i);
    buffer[i++] = '.';
    
    while (decimal) {
        temp[j++] = '0' + decimal % 10;
        decimal /= 10;
    }
    
    temp[j] = '\0';
    
    reverseString(temp, j);
    j = 0;
    
    while (j < 6) {
        buffer[i++] = temp[j];
        j++;
    }
    
    
    buffer[i] = '\0';
    
    return buffer;
}


// A copy of the arduino map function
long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/* [] END OF FILE */
