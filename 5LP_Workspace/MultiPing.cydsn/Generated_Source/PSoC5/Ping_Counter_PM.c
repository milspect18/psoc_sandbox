/*******************************************************************************
* File Name: Ping_Counter_PM.c  
* Version 3.0
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "Ping_Counter.h"

static Ping_Counter_backupStruct Ping_Counter_backup;


/*******************************************************************************
* Function Name: Ping_Counter_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Ping_Counter_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void Ping_Counter_SaveConfig(void) 
{
    #if (!Ping_Counter_UsingFixedFunction)

        Ping_Counter_backup.CounterUdb = Ping_Counter_ReadCounter();

        #if(!Ping_Counter_ControlRegRemoved)
            Ping_Counter_backup.CounterControlRegister = Ping_Counter_ReadControlRegister();
        #endif /* (!Ping_Counter_ControlRegRemoved) */

    #endif /* (!Ping_Counter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Ping_Counter_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Ping_Counter_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void Ping_Counter_RestoreConfig(void) 
{      
    #if (!Ping_Counter_UsingFixedFunction)

       Ping_Counter_WriteCounter(Ping_Counter_backup.CounterUdb);

        #if(!Ping_Counter_ControlRegRemoved)
            Ping_Counter_WriteControlRegister(Ping_Counter_backup.CounterControlRegister);
        #endif /* (!Ping_Counter_ControlRegRemoved) */

    #endif /* (!Ping_Counter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Ping_Counter_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Ping_Counter_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void Ping_Counter_Sleep(void) 
{
    #if(!Ping_Counter_ControlRegRemoved)
        /* Save Counter's enable state */
        if(Ping_Counter_CTRL_ENABLE == (Ping_Counter_CONTROL & Ping_Counter_CTRL_ENABLE))
        {
            /* Counter is enabled */
            Ping_Counter_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            Ping_Counter_backup.CounterEnableState = 0u;
        }
    #else
        Ping_Counter_backup.CounterEnableState = 1u;
        if(Ping_Counter_backup.CounterEnableState != 0u)
        {
            Ping_Counter_backup.CounterEnableState = 0u;
        }
    #endif /* (!Ping_Counter_ControlRegRemoved) */
    
    Ping_Counter_Stop();
    Ping_Counter_SaveConfig();
}


/*******************************************************************************
* Function Name: Ping_Counter_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Ping_Counter_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void Ping_Counter_Wakeup(void) 
{
    Ping_Counter_RestoreConfig();
    #if(!Ping_Counter_ControlRegRemoved)
        if(Ping_Counter_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            Ping_Counter_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!Ping_Counter_ControlRegRemoved) */
    
}


/* [] END OF FILE */
