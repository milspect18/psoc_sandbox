/*******************************************************************************
* File Name: Ping_Timer_PM.c  
* Version 3.0
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "Ping_Timer.h"

static Ping_Timer_backupStruct Ping_Timer_backup;


/*******************************************************************************
* Function Name: Ping_Timer_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Ping_Timer_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void Ping_Timer_SaveConfig(void) 
{
    #if (!Ping_Timer_UsingFixedFunction)

        Ping_Timer_backup.CounterUdb = Ping_Timer_ReadCounter();

        #if(!Ping_Timer_ControlRegRemoved)
            Ping_Timer_backup.CounterControlRegister = Ping_Timer_ReadControlRegister();
        #endif /* (!Ping_Timer_ControlRegRemoved) */

    #endif /* (!Ping_Timer_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Ping_Timer_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Ping_Timer_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void Ping_Timer_RestoreConfig(void) 
{      
    #if (!Ping_Timer_UsingFixedFunction)

       Ping_Timer_WriteCounter(Ping_Timer_backup.CounterUdb);

        #if(!Ping_Timer_ControlRegRemoved)
            Ping_Timer_WriteControlRegister(Ping_Timer_backup.CounterControlRegister);
        #endif /* (!Ping_Timer_ControlRegRemoved) */

    #endif /* (!Ping_Timer_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Ping_Timer_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Ping_Timer_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void Ping_Timer_Sleep(void) 
{
    #if(!Ping_Timer_ControlRegRemoved)
        /* Save Counter's enable state */
        if(Ping_Timer_CTRL_ENABLE == (Ping_Timer_CONTROL & Ping_Timer_CTRL_ENABLE))
        {
            /* Counter is enabled */
            Ping_Timer_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            Ping_Timer_backup.CounterEnableState = 0u;
        }
    #else
        Ping_Timer_backup.CounterEnableState = 1u;
        if(Ping_Timer_backup.CounterEnableState != 0u)
        {
            Ping_Timer_backup.CounterEnableState = 0u;
        }
    #endif /* (!Ping_Timer_ControlRegRemoved) */
    
    Ping_Timer_Stop();
    Ping_Timer_SaveConfig();
}


/*******************************************************************************
* Function Name: Ping_Timer_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Ping_Timer_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void Ping_Timer_Wakeup(void) 
{
    Ping_Timer_RestoreConfig();
    #if(!Ping_Timer_ControlRegRemoved)
        if(Ping_Timer_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            Ping_Timer_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!Ping_Timer_ControlRegRemoved) */
    
}


/* [] END OF FILE */
