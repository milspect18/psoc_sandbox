/*******************************************************************************
* File Name: Ping_Timer.h  
* Version 3.0
*
*  Description:
*   Contains the function prototypes and constants available to the counter
*   user module.
*
*   Note:
*    None
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
    
#if !defined(CY_COUNTER_Ping_Timer_H)
#define CY_COUNTER_Ping_Timer_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

extern uint8 Ping_Timer_initVar;

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component Counter_v3_0 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */

/* Error message for removed Ping_Timer_CounterUDB_sCTRLReg_ctrlreg through optimization */
#ifdef Ping_Timer_CounterUDB_sCTRLReg_ctrlreg__REMOVED
    #error Counter_v3_0 detected with a constant 0 for the enable, a \
                                constant 0 for the count or constant 1 for \
                                the reset. This will prevent the component from\
                                operating.
#endif /* Ping_Timer_CounterUDB_sCTRLReg_ctrlreg__REMOVED */


/**************************************
*           Parameter Defaults        
**************************************/

#define Ping_Timer_Resolution            16u
#define Ping_Timer_UsingFixedFunction    0u
#define Ping_Timer_ControlRegRemoved     0u
#define Ping_Timer_COMPARE_MODE_SOFTWARE 0u
#define Ping_Timer_CAPTURE_MODE_SOFTWARE 0u
#define Ping_Timer_RunModeUsed           0u


/***************************************
*       Type defines
***************************************/


/**************************************************************************
 * Sleep Mode API Support
 * Backup structure for Sleep Wake up operations
 *************************************************************************/
typedef struct
{
    uint8 CounterEnableState; 
    uint16 CounterUdb;         /* Current Counter Value */

    #if (!Ping_Timer_ControlRegRemoved)
        uint8 CounterControlRegister;               /* Counter Control Register */
    #endif /* (!Ping_Timer_ControlRegRemoved) */

}Ping_Timer_backupStruct;


/**************************************
 *  Function Prototypes
 *************************************/
void    Ping_Timer_Start(void) ;
void    Ping_Timer_Stop(void) ;
void    Ping_Timer_SetInterruptMode(uint8 interruptsMask) ;
uint8   Ping_Timer_ReadStatusRegister(void) ;
#define Ping_Timer_GetInterruptSource() Ping_Timer_ReadStatusRegister()
#if(!Ping_Timer_ControlRegRemoved)
    uint8   Ping_Timer_ReadControlRegister(void) ;
    void    Ping_Timer_WriteControlRegister(uint8 control) \
        ;
#endif /* (!Ping_Timer_ControlRegRemoved) */
#if (!(Ping_Timer_UsingFixedFunction && (CY_PSOC5A)))
    void    Ping_Timer_WriteCounter(uint16 counter) \
            ; 
#endif /* (!(Ping_Timer_UsingFixedFunction && (CY_PSOC5A))) */
uint16  Ping_Timer_ReadCounter(void) ;
uint16  Ping_Timer_ReadCapture(void) ;
void    Ping_Timer_WritePeriod(uint16 period) \
    ;
uint16  Ping_Timer_ReadPeriod( void ) ;
#if (!Ping_Timer_UsingFixedFunction)
    void    Ping_Timer_WriteCompare(uint16 compare) \
        ;
    uint16  Ping_Timer_ReadCompare( void ) \
        ;
#endif /* (!Ping_Timer_UsingFixedFunction) */

#if (Ping_Timer_COMPARE_MODE_SOFTWARE)
    void    Ping_Timer_SetCompareMode(uint8 compareMode) ;
#endif /* (Ping_Timer_COMPARE_MODE_SOFTWARE) */
#if (Ping_Timer_CAPTURE_MODE_SOFTWARE)
    void    Ping_Timer_SetCaptureMode(uint8 captureMode) ;
#endif /* (Ping_Timer_CAPTURE_MODE_SOFTWARE) */
void Ping_Timer_ClearFIFO(void)     ;
void Ping_Timer_Init(void)          ;
void Ping_Timer_Enable(void)        ;
void Ping_Timer_SaveConfig(void)    ;
void Ping_Timer_RestoreConfig(void) ;
void Ping_Timer_Sleep(void)         ;
void Ping_Timer_Wakeup(void)        ;


/***************************************
*   Enumerated Types and Parameters
***************************************/

/* Enumerated Type B_Counter__CompareModes, Used in Compare Mode retained for backward compatibility of tests*/
#define Ping_Timer__B_COUNTER__LESS_THAN 1
#define Ping_Timer__B_COUNTER__LESS_THAN_OR_EQUAL 2
#define Ping_Timer__B_COUNTER__EQUAL 0
#define Ping_Timer__B_COUNTER__GREATER_THAN 3
#define Ping_Timer__B_COUNTER__GREATER_THAN_OR_EQUAL 4
#define Ping_Timer__B_COUNTER__SOFTWARE 5

/* Enumerated Type Counter_CompareModes */
#define Ping_Timer_CMP_MODE_LT 1u
#define Ping_Timer_CMP_MODE_LTE 2u
#define Ping_Timer_CMP_MODE_EQ 0u
#define Ping_Timer_CMP_MODE_GT 3u
#define Ping_Timer_CMP_MODE_GTE 4u
#define Ping_Timer_CMP_MODE_SOFTWARE_CONTROLLED 5u

/* Enumerated Type B_Counter__CaptureModes, Used in Capture Mode retained for backward compatibility of tests*/
#define Ping_Timer__B_COUNTER__NONE 0
#define Ping_Timer__B_COUNTER__RISING_EDGE 1
#define Ping_Timer__B_COUNTER__FALLING_EDGE 2
#define Ping_Timer__B_COUNTER__EITHER_EDGE 3
#define Ping_Timer__B_COUNTER__SOFTWARE_CONTROL 4

/* Enumerated Type Counter_CompareModes */
#define Ping_Timer_CAP_MODE_NONE 0u
#define Ping_Timer_CAP_MODE_RISE 1u
#define Ping_Timer_CAP_MODE_FALL 2u
#define Ping_Timer_CAP_MODE_BOTH 3u
#define Ping_Timer_CAP_MODE_SOFTWARE_CONTROLLED 4u


/***************************************
 *  Initialization Values
 **************************************/
#define Ping_Timer_CAPTURE_MODE_CONF       0u
#define Ping_Timer_INIT_PERIOD_VALUE       65535u
#define Ping_Timer_INIT_COUNTER_VALUE      0u
#if (Ping_Timer_UsingFixedFunction)
#define Ping_Timer_INIT_INTERRUPTS_MASK    ((uint8)((uint8)0u << Ping_Timer_STATUS_ZERO_INT_EN_MASK_SHIFT))
#else
#define Ping_Timer_INIT_COMPARE_VALUE      65535u
#define Ping_Timer_INIT_INTERRUPTS_MASK ((uint8)((uint8)0u << Ping_Timer_STATUS_ZERO_INT_EN_MASK_SHIFT) | \
        ((uint8)((uint8)0u << Ping_Timer_STATUS_CAPTURE_INT_EN_MASK_SHIFT)) | \
        ((uint8)((uint8)0u << Ping_Timer_STATUS_CMP_INT_EN_MASK_SHIFT)) | \
        ((uint8)((uint8)0u << Ping_Timer_STATUS_OVERFLOW_INT_EN_MASK_SHIFT)) | \
        ((uint8)((uint8)0u << Ping_Timer_STATUS_UNDERFLOW_INT_EN_MASK_SHIFT)))
#define Ping_Timer_DEFAULT_COMPARE_MODE    1u

#if( 0 != Ping_Timer_CAPTURE_MODE_CONF)
    #define Ping_Timer_DEFAULT_CAPTURE_MODE    ((uint8)((uint8)0u << Ping_Timer_CTRL_CAPMODE0_SHIFT))
#else    
    #define Ping_Timer_DEFAULT_CAPTURE_MODE    (0u )
#endif /* ( 0 != Ping_Timer_CAPTURE_MODE_CONF) */

#endif /* (Ping_Timer_UsingFixedFunction) */


/**************************************
 *  Registers
 *************************************/
#if (Ping_Timer_UsingFixedFunction)
    #define Ping_Timer_STATICCOUNT_LSB     (*(reg16 *) Ping_Timer_CounterHW__CAP0 )
    #define Ping_Timer_STATICCOUNT_LSB_PTR ( (reg16 *) Ping_Timer_CounterHW__CAP0 )
    #define Ping_Timer_PERIOD_LSB          (*(reg16 *) Ping_Timer_CounterHW__PER0 )
    #define Ping_Timer_PERIOD_LSB_PTR      ( (reg16 *) Ping_Timer_CounterHW__PER0 )
    /* MODE must be set to 1 to set the compare value */
    #define Ping_Timer_COMPARE_LSB         (*(reg16 *) Ping_Timer_CounterHW__CNT_CMP0 )
    #define Ping_Timer_COMPARE_LSB_PTR     ( (reg16 *) Ping_Timer_CounterHW__CNT_CMP0 )
    /* MODE must be set to 0 to get the count */
    #define Ping_Timer_COUNTER_LSB         (*(reg16 *) Ping_Timer_CounterHW__CNT_CMP0 )
    #define Ping_Timer_COUNTER_LSB_PTR     ( (reg16 *) Ping_Timer_CounterHW__CNT_CMP0 )
    #define Ping_Timer_RT1                 (*(reg8 *) Ping_Timer_CounterHW__RT1)
    #define Ping_Timer_RT1_PTR             ( (reg8 *) Ping_Timer_CounterHW__RT1)
#else
    
    #if (Ping_Timer_Resolution <= 8u) /* 8-bit Counter */
    
        #define Ping_Timer_STATICCOUNT_LSB     (*(reg8 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__F0_REG )
        #define Ping_Timer_STATICCOUNT_LSB_PTR ( (reg8 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__F0_REG )
        #define Ping_Timer_PERIOD_LSB          (*(reg8 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__D0_REG )
        #define Ping_Timer_PERIOD_LSB_PTR      ( (reg8 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__D0_REG )
        #define Ping_Timer_COMPARE_LSB         (*(reg8 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__D1_REG )
        #define Ping_Timer_COMPARE_LSB_PTR     ( (reg8 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__D1_REG )
        #define Ping_Timer_COUNTER_LSB         (*(reg8 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__A0_REG )  
        #define Ping_Timer_COUNTER_LSB_PTR     ( (reg8 *)\
            Ping_Timer_CounterUDB_sC16_counterdp_u0__A0_REG )
    
    #elif(Ping_Timer_Resolution <= 16u) /* 16-bit Counter */
        #if(CY_PSOC3) /* 8-bit address space */ 
            #define Ping_Timer_STATICCOUNT_LSB     (*(reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__F0_REG )
            #define Ping_Timer_STATICCOUNT_LSB_PTR ( (reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__F0_REG )
            #define Ping_Timer_PERIOD_LSB          (*(reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__D0_REG )
            #define Ping_Timer_PERIOD_LSB_PTR      ( (reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__D0_REG )
            #define Ping_Timer_COMPARE_LSB         (*(reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__D1_REG )
            #define Ping_Timer_COMPARE_LSB_PTR     ( (reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__D1_REG )
            #define Ping_Timer_COUNTER_LSB         (*(reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__A0_REG )  
            #define Ping_Timer_COUNTER_LSB_PTR     ( (reg16 *)\
                Ping_Timer_CounterUDB_sC16_counterdp_u0__A0_REG )
        #else /* 16-bit address space */
            #define Ping_Timer_STATICCOUNT_LSB     (*(reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__16BIT_F0_REG )
            #define Ping_Timer_STATICCOUNT_LSB_PTR ( (reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__16BIT_F0_REG )
            #define Ping_Timer_PERIOD_LSB          (*(reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__16BIT_D0_REG )
            #define Ping_Timer_PERIOD_LSB_PTR      ( (reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__16BIT_D0_REG )
            #define Ping_Timer_COMPARE_LSB         (*(reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__16BIT_D1_REG )
            #define Ping_Timer_COMPARE_LSB_PTR     ( (reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__16BIT_D1_REG )
            #define Ping_Timer_COUNTER_LSB         (*(reg16 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__16BIT_A0_REG )  
            #define Ping_Timer_COUNTER_LSB_PTR     ( (reg16 *)\
                Ping_Timer_CounterUDB_sC16_counterdp_u0__16BIT_A0_REG )
        #endif /* CY_PSOC3 */   
    #elif(Ping_Timer_Resolution <= 24u) /* 24-bit Counter */
        
        #define Ping_Timer_STATICCOUNT_LSB     (*(reg32 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__F0_REG )
        #define Ping_Timer_STATICCOUNT_LSB_PTR ( (reg32 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__F0_REG )
        #define Ping_Timer_PERIOD_LSB          (*(reg32 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__D0_REG )
        #define Ping_Timer_PERIOD_LSB_PTR      ( (reg32 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__D0_REG )
        #define Ping_Timer_COMPARE_LSB         (*(reg32 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__D1_REG )
        #define Ping_Timer_COMPARE_LSB_PTR     ( (reg32 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__D1_REG )
        #define Ping_Timer_COUNTER_LSB         (*(reg32 *) \
            Ping_Timer_CounterUDB_sC16_counterdp_u0__A0_REG )  
        #define Ping_Timer_COUNTER_LSB_PTR     ( (reg32 *)\
            Ping_Timer_CounterUDB_sC16_counterdp_u0__A0_REG )
    
    #else /* 32-bit Counter */
        #if(CY_PSOC3 || CY_PSOC5) /* 8-bit address space */
            #define Ping_Timer_STATICCOUNT_LSB     (*(reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__F0_REG )
            #define Ping_Timer_STATICCOUNT_LSB_PTR ( (reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__F0_REG )
            #define Ping_Timer_PERIOD_LSB          (*(reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__D0_REG )
            #define Ping_Timer_PERIOD_LSB_PTR      ( (reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__D0_REG )
            #define Ping_Timer_COMPARE_LSB         (*(reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__D1_REG )
            #define Ping_Timer_COMPARE_LSB_PTR     ( (reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__D1_REG )
            #define Ping_Timer_COUNTER_LSB         (*(reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__A0_REG )  
            #define Ping_Timer_COUNTER_LSB_PTR     ( (reg32 *)\
                Ping_Timer_CounterUDB_sC16_counterdp_u0__A0_REG )
        #else /* 32-bit address space */
            #define Ping_Timer_STATICCOUNT_LSB     (*(reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__32BIT_F0_REG )
            #define Ping_Timer_STATICCOUNT_LSB_PTR ( (reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__32BIT_F0_REG )
            #define Ping_Timer_PERIOD_LSB          (*(reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__32BIT_D0_REG )
            #define Ping_Timer_PERIOD_LSB_PTR      ( (reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__32BIT_D0_REG )
            #define Ping_Timer_COMPARE_LSB         (*(reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__32BIT_D1_REG )
            #define Ping_Timer_COMPARE_LSB_PTR     ( (reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__32BIT_D1_REG )
            #define Ping_Timer_COUNTER_LSB         (*(reg32 *) \
                Ping_Timer_CounterUDB_sC16_counterdp_u0__32BIT_A0_REG )  
            #define Ping_Timer_COUNTER_LSB_PTR     ( (reg32 *)\
                Ping_Timer_CounterUDB_sC16_counterdp_u0__32BIT_A0_REG )
        #endif /* CY_PSOC3 || CY_PSOC5 */   
    #endif

	#define Ping_Timer_COUNTER_LSB_PTR_8BIT     ( (reg8 *)\
                Ping_Timer_CounterUDB_sC16_counterdp_u0__A0_REG )
				
    #define Ping_Timer_AUX_CONTROLDP0 \
        (*(reg8 *) Ping_Timer_CounterUDB_sC16_counterdp_u0__DP_AUX_CTL_REG)
    
    #define Ping_Timer_AUX_CONTROLDP0_PTR \
        ( (reg8 *) Ping_Timer_CounterUDB_sC16_counterdp_u0__DP_AUX_CTL_REG)
    
    #if (Ping_Timer_Resolution == 16 || Ping_Timer_Resolution == 24 || Ping_Timer_Resolution == 32)
       #define Ping_Timer_AUX_CONTROLDP1 \
           (*(reg8 *) Ping_Timer_CounterUDB_sC16_counterdp_u1__DP_AUX_CTL_REG)
       #define Ping_Timer_AUX_CONTROLDP1_PTR \
           ( (reg8 *) Ping_Timer_CounterUDB_sC16_counterdp_u1__DP_AUX_CTL_REG)
    #endif /* (Ping_Timer_Resolution == 16 || Ping_Timer_Resolution == 24 || Ping_Timer_Resolution == 32) */
    
    #if (Ping_Timer_Resolution == 24 || Ping_Timer_Resolution == 32)
       #define Ping_Timer_AUX_CONTROLDP2 \
           (*(reg8 *) Ping_Timer_CounterUDB_sC16_counterdp_u2__DP_AUX_CTL_REG)
       #define Ping_Timer_AUX_CONTROLDP2_PTR \
           ( (reg8 *) Ping_Timer_CounterUDB_sC16_counterdp_u2__DP_AUX_CTL_REG)
    #endif /* (Ping_Timer_Resolution == 24 || Ping_Timer_Resolution == 32) */
    
    #if (Ping_Timer_Resolution == 32)
       #define Ping_Timer_AUX_CONTROLDP3 \
           (*(reg8 *) Ping_Timer_CounterUDB_sC16_counterdp_u3__DP_AUX_CTL_REG)
       #define Ping_Timer_AUX_CONTROLDP3_PTR \
           ( (reg8 *) Ping_Timer_CounterUDB_sC16_counterdp_u3__DP_AUX_CTL_REG)
    #endif /* (Ping_Timer_Resolution == 32) */

#endif  /* (Ping_Timer_UsingFixedFunction) */

#if (Ping_Timer_UsingFixedFunction)
    #define Ping_Timer_STATUS         (*(reg8 *) Ping_Timer_CounterHW__SR0 )
    /* In Fixed Function Block Status and Mask are the same register */
    #define Ping_Timer_STATUS_MASK             (*(reg8 *) Ping_Timer_CounterHW__SR0 )
    #define Ping_Timer_STATUS_MASK_PTR         ( (reg8 *) Ping_Timer_CounterHW__SR0 )
    #define Ping_Timer_CONTROL                 (*(reg8 *) Ping_Timer_CounterHW__CFG0)
    #define Ping_Timer_CONTROL_PTR             ( (reg8 *) Ping_Timer_CounterHW__CFG0)
    #define Ping_Timer_CONTROL2                (*(reg8 *) Ping_Timer_CounterHW__CFG1)
    #define Ping_Timer_CONTROL2_PTR            ( (reg8 *) Ping_Timer_CounterHW__CFG1)
    #if (CY_PSOC3 || CY_PSOC5LP)
        #define Ping_Timer_CONTROL3       (*(reg8 *) Ping_Timer_CounterHW__CFG2)
        #define Ping_Timer_CONTROL3_PTR   ( (reg8 *) Ping_Timer_CounterHW__CFG2)
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    #define Ping_Timer_GLOBAL_ENABLE           (*(reg8 *) Ping_Timer_CounterHW__PM_ACT_CFG)
    #define Ping_Timer_GLOBAL_ENABLE_PTR       ( (reg8 *) Ping_Timer_CounterHW__PM_ACT_CFG)
    #define Ping_Timer_GLOBAL_STBY_ENABLE      (*(reg8 *) Ping_Timer_CounterHW__PM_STBY_CFG)
    #define Ping_Timer_GLOBAL_STBY_ENABLE_PTR  ( (reg8 *) Ping_Timer_CounterHW__PM_STBY_CFG)
    

    /********************************
    *    Constants
    ********************************/

    /* Fixed Function Block Chosen */
    #define Ping_Timer_BLOCK_EN_MASK          Ping_Timer_CounterHW__PM_ACT_MSK
    #define Ping_Timer_BLOCK_STBY_EN_MASK     Ping_Timer_CounterHW__PM_STBY_MSK 
    
    /* Control Register Bit Locations */    
    /* As defined in Register Map, part of TMRX_CFG0 register */
    #define Ping_Timer_CTRL_ENABLE_SHIFT      0x00u
    #define Ping_Timer_ONESHOT_SHIFT          0x02u
    /* Control Register Bit Masks */
    #define Ping_Timer_CTRL_ENABLE            ((uint8)((uint8)0x01u << Ping_Timer_CTRL_ENABLE_SHIFT))         
    #define Ping_Timer_ONESHOT                ((uint8)((uint8)0x01u << Ping_Timer_ONESHOT_SHIFT))

    /* Control2 Register Bit Masks */
    /* Set the mask for run mode */
    #if (CY_PSOC5A)
        /* Use CFG1 Mode bits to set run mode */
        #define Ping_Timer_CTRL_MODE_SHIFT        0x01u    
        #define Ping_Timer_CTRL_MODE_MASK         ((uint8)((uint8)0x07u << Ping_Timer_CTRL_MODE_SHIFT))
    #endif /* (CY_PSOC5A) */
    #if (CY_PSOC3 || CY_PSOC5LP)
        /* Use CFG2 Mode bits to set run mode */
        #define Ping_Timer_CTRL_MODE_SHIFT        0x00u    
        #define Ping_Timer_CTRL_MODE_MASK         ((uint8)((uint8)0x03u << Ping_Timer_CTRL_MODE_SHIFT))
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    /* Set the mask for interrupt (raw/status register) */
    #define Ping_Timer_CTRL2_IRQ_SEL_SHIFT     0x00u
    #define Ping_Timer_CTRL2_IRQ_SEL          ((uint8)((uint8)0x01u << Ping_Timer_CTRL2_IRQ_SEL_SHIFT))     
    
    /* Status Register Bit Locations */
    #define Ping_Timer_STATUS_ZERO_SHIFT      0x07u  /* As defined in Register Map, part of TMRX_SR0 register */ 

    /* Status Register Interrupt Enable Bit Locations */
    #define Ping_Timer_STATUS_ZERO_INT_EN_MASK_SHIFT      (Ping_Timer_STATUS_ZERO_SHIFT - 0x04u)

    /* Status Register Bit Masks */                           
    #define Ping_Timer_STATUS_ZERO            ((uint8)((uint8)0x01u << Ping_Timer_STATUS_ZERO_SHIFT))

    /* Status Register Interrupt Bit Masks*/
    #define Ping_Timer_STATUS_ZERO_INT_EN_MASK       ((uint8)((uint8)Ping_Timer_STATUS_ZERO >> 0x04u))
    
    /*RT1 Synch Constants: Applicable for PSoC3 and PSoC5LP */
    #define Ping_Timer_RT1_SHIFT            0x04u
    #define Ping_Timer_RT1_MASK             ((uint8)((uint8)0x03u << Ping_Timer_RT1_SHIFT))  /* Sync TC and CMP bit masks */
    #define Ping_Timer_SYNC                 ((uint8)((uint8)0x03u << Ping_Timer_RT1_SHIFT))
    #define Ping_Timer_SYNCDSI_SHIFT        0x00u
    #define Ping_Timer_SYNCDSI_MASK         ((uint8)((uint8)0x0Fu << Ping_Timer_SYNCDSI_SHIFT)) /* Sync all DSI inputs */
    #define Ping_Timer_SYNCDSI_EN           ((uint8)((uint8)0x0Fu << Ping_Timer_SYNCDSI_SHIFT)) /* Sync all DSI inputs */
    
#else /* !Ping_Timer_UsingFixedFunction */
    #define Ping_Timer_STATUS               (* (reg8 *) Ping_Timer_CounterUDB_sSTSReg_stsreg__STATUS_REG )
    #define Ping_Timer_STATUS_PTR           (  (reg8 *) Ping_Timer_CounterUDB_sSTSReg_stsreg__STATUS_REG )
    #define Ping_Timer_STATUS_MASK          (* (reg8 *) Ping_Timer_CounterUDB_sSTSReg_stsreg__MASK_REG )
    #define Ping_Timer_STATUS_MASK_PTR      (  (reg8 *) Ping_Timer_CounterUDB_sSTSReg_stsreg__MASK_REG )
    #define Ping_Timer_STATUS_AUX_CTRL      (*(reg8 *) Ping_Timer_CounterUDB_sSTSReg_stsreg__STATUS_AUX_CTL_REG)
    #define Ping_Timer_STATUS_AUX_CTRL_PTR  ( (reg8 *) Ping_Timer_CounterUDB_sSTSReg_stsreg__STATUS_AUX_CTL_REG)
    #define Ping_Timer_CONTROL              (* (reg8 *) Ping_Timer_CounterUDB_sCTRLReg_ctrlreg__CONTROL_REG )
    #define Ping_Timer_CONTROL_PTR          (  (reg8 *) Ping_Timer_CounterUDB_sCTRLReg_ctrlreg__CONTROL_REG )


    /********************************
    *    Constants
    ********************************/
    /* Control Register Bit Locations */
    #define Ping_Timer_CTRL_CAPMODE0_SHIFT    0x03u       /* As defined by Verilog Implementation */
    #define Ping_Timer_CTRL_RESET_SHIFT       0x06u       /* As defined by Verilog Implementation */
    #define Ping_Timer_CTRL_ENABLE_SHIFT      0x07u       /* As defined by Verilog Implementation */
    /* Control Register Bit Masks */
    #define Ping_Timer_CTRL_CMPMODE_MASK      0x07u 
    #define Ping_Timer_CTRL_CAPMODE_MASK      0x03u  
    #define Ping_Timer_CTRL_RESET             ((uint8)((uint8)0x01u << Ping_Timer_CTRL_RESET_SHIFT))  
    #define Ping_Timer_CTRL_ENABLE            ((uint8)((uint8)0x01u << Ping_Timer_CTRL_ENABLE_SHIFT)) 

    /* Status Register Bit Locations */
    #define Ping_Timer_STATUS_CMP_SHIFT       0x00u       /* As defined by Verilog Implementation */
    #define Ping_Timer_STATUS_ZERO_SHIFT      0x01u       /* As defined by Verilog Implementation */
    #define Ping_Timer_STATUS_OVERFLOW_SHIFT  0x02u       /* As defined by Verilog Implementation */
    #define Ping_Timer_STATUS_UNDERFLOW_SHIFT 0x03u       /* As defined by Verilog Implementation */
    #define Ping_Timer_STATUS_CAPTURE_SHIFT   0x04u       /* As defined by Verilog Implementation */
    #define Ping_Timer_STATUS_FIFOFULL_SHIFT  0x05u       /* As defined by Verilog Implementation */
    #define Ping_Timer_STATUS_FIFONEMP_SHIFT  0x06u       /* As defined by Verilog Implementation */
    /* Status Register Interrupt Enable Bit Locations - UDB Status Interrupt Mask match Status Bit Locations*/
    #define Ping_Timer_STATUS_CMP_INT_EN_MASK_SHIFT       Ping_Timer_STATUS_CMP_SHIFT       
    #define Ping_Timer_STATUS_ZERO_INT_EN_MASK_SHIFT      Ping_Timer_STATUS_ZERO_SHIFT      
    #define Ping_Timer_STATUS_OVERFLOW_INT_EN_MASK_SHIFT  Ping_Timer_STATUS_OVERFLOW_SHIFT  
    #define Ping_Timer_STATUS_UNDERFLOW_INT_EN_MASK_SHIFT Ping_Timer_STATUS_UNDERFLOW_SHIFT 
    #define Ping_Timer_STATUS_CAPTURE_INT_EN_MASK_SHIFT   Ping_Timer_STATUS_CAPTURE_SHIFT   
    #define Ping_Timer_STATUS_FIFOFULL_INT_EN_MASK_SHIFT  Ping_Timer_STATUS_FIFOFULL_SHIFT  
    #define Ping_Timer_STATUS_FIFONEMP_INT_EN_MASK_SHIFT  Ping_Timer_STATUS_FIFONEMP_SHIFT  
    /* Status Register Bit Masks */                
    #define Ping_Timer_STATUS_CMP             ((uint8)((uint8)0x01u << Ping_Timer_STATUS_CMP_SHIFT))  
    #define Ping_Timer_STATUS_ZERO            ((uint8)((uint8)0x01u << Ping_Timer_STATUS_ZERO_SHIFT)) 
    #define Ping_Timer_STATUS_OVERFLOW        ((uint8)((uint8)0x01u << Ping_Timer_STATUS_OVERFLOW_SHIFT)) 
    #define Ping_Timer_STATUS_UNDERFLOW       ((uint8)((uint8)0x01u << Ping_Timer_STATUS_UNDERFLOW_SHIFT)) 
    #define Ping_Timer_STATUS_CAPTURE         ((uint8)((uint8)0x01u << Ping_Timer_STATUS_CAPTURE_SHIFT)) 
    #define Ping_Timer_STATUS_FIFOFULL        ((uint8)((uint8)0x01u << Ping_Timer_STATUS_FIFOFULL_SHIFT))
    #define Ping_Timer_STATUS_FIFONEMP        ((uint8)((uint8)0x01u << Ping_Timer_STATUS_FIFONEMP_SHIFT))
    /* Status Register Interrupt Bit Masks  - UDB Status Interrupt Mask match Status Bit Locations */
    #define Ping_Timer_STATUS_CMP_INT_EN_MASK            Ping_Timer_STATUS_CMP                    
    #define Ping_Timer_STATUS_ZERO_INT_EN_MASK           Ping_Timer_STATUS_ZERO            
    #define Ping_Timer_STATUS_OVERFLOW_INT_EN_MASK       Ping_Timer_STATUS_OVERFLOW        
    #define Ping_Timer_STATUS_UNDERFLOW_INT_EN_MASK      Ping_Timer_STATUS_UNDERFLOW       
    #define Ping_Timer_STATUS_CAPTURE_INT_EN_MASK        Ping_Timer_STATUS_CAPTURE         
    #define Ping_Timer_STATUS_FIFOFULL_INT_EN_MASK       Ping_Timer_STATUS_FIFOFULL        
    #define Ping_Timer_STATUS_FIFONEMP_INT_EN_MASK       Ping_Timer_STATUS_FIFONEMP         
    

    /* StatusI Interrupt Enable bit Location in the Auxilliary Control Register */
    #define Ping_Timer_STATUS_ACTL_INT_EN     0x10u /* As defined for the ACTL Register */
    
    /* Datapath Auxillary Control Register definitions */
    #define Ping_Timer_AUX_CTRL_FIFO0_CLR         0x01u   /* As defined by Register map */
    #define Ping_Timer_AUX_CTRL_FIFO1_CLR         0x02u   /* As defined by Register map */
    #define Ping_Timer_AUX_CTRL_FIFO0_LVL         0x04u   /* As defined by Register map */
    #define Ping_Timer_AUX_CTRL_FIFO1_LVL         0x08u   /* As defined by Register map */
    #define Ping_Timer_STATUS_ACTL_INT_EN_MASK    0x10u   /* As defined for the ACTL Register */
    
#endif /* Ping_Timer_UsingFixedFunction */

#endif  /* CY_COUNTER_Ping_Timer_H */


/* [] END OF FILE */

