/*******************************************************************************
* File Name: Timeout_Timer_PM.c
* Version 2.70
*
*  Description:
*     This file provides the power management source code to API for the
*     Timer.
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "Timeout_Timer.h"

static Timeout_Timer_backupStruct Timeout_Timer_backup;


/*******************************************************************************
* Function Name: Timeout_Timer_SaveConfig
********************************************************************************
*
* Summary:
*     Save the current user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Timeout_Timer_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void Timeout_Timer_SaveConfig(void) 
{
    #if (!Timeout_Timer_UsingFixedFunction)
        Timeout_Timer_backup.TimerUdb = Timeout_Timer_ReadCounter();
        Timeout_Timer_backup.InterruptMaskValue = Timeout_Timer_STATUS_MASK;
        #if (Timeout_Timer_UsingHWCaptureCounter)
            Timeout_Timer_backup.TimerCaptureCounter = Timeout_Timer_ReadCaptureCount();
        #endif /* Back Up capture counter register  */

        #if(!Timeout_Timer_UDB_CONTROL_REG_REMOVED)
            Timeout_Timer_backup.TimerControlRegister = Timeout_Timer_ReadControlRegister();
        #endif /* Backup the enable state of the Timer component */
    #endif /* Backup non retention registers in UDB implementation. All fixed function registers are retention */
}


/*******************************************************************************
* Function Name: Timeout_Timer_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Timeout_Timer_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void Timeout_Timer_RestoreConfig(void) 
{   
    #if (!Timeout_Timer_UsingFixedFunction)

        Timeout_Timer_WriteCounter(Timeout_Timer_backup.TimerUdb);
        Timeout_Timer_STATUS_MASK =Timeout_Timer_backup.InterruptMaskValue;
        #if (Timeout_Timer_UsingHWCaptureCounter)
            Timeout_Timer_SetCaptureCount(Timeout_Timer_backup.TimerCaptureCounter);
        #endif /* Restore Capture counter register*/

        #if(!Timeout_Timer_UDB_CONTROL_REG_REMOVED)
            Timeout_Timer_WriteControlRegister(Timeout_Timer_backup.TimerControlRegister);
        #endif /* Restore the enable state of the Timer component */
    #endif /* Restore non retention registers in the UDB implementation only */
}


/*******************************************************************************
* Function Name: Timeout_Timer_Sleep
********************************************************************************
*
* Summary:
*     Stop and Save the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Timeout_Timer_backup.TimerEnableState:  Is modified depending on the
*  enable state of the block before entering sleep mode.
*
*******************************************************************************/
void Timeout_Timer_Sleep(void) 
{
    #if(!Timeout_Timer_UDB_CONTROL_REG_REMOVED)
        /* Save Counter's enable state */
        if(Timeout_Timer_CTRL_ENABLE == (Timeout_Timer_CONTROL & Timeout_Timer_CTRL_ENABLE))
        {
            /* Timer is enabled */
            Timeout_Timer_backup.TimerEnableState = 1u;
        }
        else
        {
            /* Timer is disabled */
            Timeout_Timer_backup.TimerEnableState = 0u;
        }
    #endif /* Back up enable state from the Timer control register */
    Timeout_Timer_Stop();
    Timeout_Timer_SaveConfig();
}


/*******************************************************************************
* Function Name: Timeout_Timer_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Timeout_Timer_backup.enableState:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void Timeout_Timer_Wakeup(void) 
{
    Timeout_Timer_RestoreConfig();
    #if(!Timeout_Timer_UDB_CONTROL_REG_REMOVED)
        if(Timeout_Timer_backup.TimerEnableState == 1u)
        {     /* Enable Timer's operation */
                Timeout_Timer_Enable();
        } /* Do nothing if Timer was disabled before */
    #endif /* Remove this code section if Control register is removed */
}


/* [] END OF FILE */
