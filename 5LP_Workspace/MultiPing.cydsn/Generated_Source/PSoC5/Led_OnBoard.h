/*******************************************************************************
* File Name: Led_OnBoard.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Led_OnBoard_H) /* Pins Led_OnBoard_H */
#define CY_PINS_Led_OnBoard_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Led_OnBoard_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Led_OnBoard__PORT == 15 && ((Led_OnBoard__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Led_OnBoard_Write(uint8 value);
void    Led_OnBoard_SetDriveMode(uint8 mode);
uint8   Led_OnBoard_ReadDataReg(void);
uint8   Led_OnBoard_Read(void);
void    Led_OnBoard_SetInterruptMode(uint16 position, uint16 mode);
uint8   Led_OnBoard_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Led_OnBoard_SetDriveMode() function.
     *  @{
     */
        #define Led_OnBoard_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Led_OnBoard_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Led_OnBoard_DM_RES_UP          PIN_DM_RES_UP
        #define Led_OnBoard_DM_RES_DWN         PIN_DM_RES_DWN
        #define Led_OnBoard_DM_OD_LO           PIN_DM_OD_LO
        #define Led_OnBoard_DM_OD_HI           PIN_DM_OD_HI
        #define Led_OnBoard_DM_STRONG          PIN_DM_STRONG
        #define Led_OnBoard_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Led_OnBoard_MASK               Led_OnBoard__MASK
#define Led_OnBoard_SHIFT              Led_OnBoard__SHIFT
#define Led_OnBoard_WIDTH              1u

/* Interrupt constants */
#if defined(Led_OnBoard__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Led_OnBoard_SetInterruptMode() function.
     *  @{
     */
        #define Led_OnBoard_INTR_NONE      (uint16)(0x0000u)
        #define Led_OnBoard_INTR_RISING    (uint16)(0x0001u)
        #define Led_OnBoard_INTR_FALLING   (uint16)(0x0002u)
        #define Led_OnBoard_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Led_OnBoard_INTR_MASK      (0x01u) 
#endif /* (Led_OnBoard__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Led_OnBoard_PS                     (* (reg8 *) Led_OnBoard__PS)
/* Data Register */
#define Led_OnBoard_DR                     (* (reg8 *) Led_OnBoard__DR)
/* Port Number */
#define Led_OnBoard_PRT_NUM                (* (reg8 *) Led_OnBoard__PRT) 
/* Connect to Analog Globals */                                                  
#define Led_OnBoard_AG                     (* (reg8 *) Led_OnBoard__AG)                       
/* Analog MUX bux enable */
#define Led_OnBoard_AMUX                   (* (reg8 *) Led_OnBoard__AMUX) 
/* Bidirectional Enable */                                                        
#define Led_OnBoard_BIE                    (* (reg8 *) Led_OnBoard__BIE)
/* Bit-mask for Aliased Register Access */
#define Led_OnBoard_BIT_MASK               (* (reg8 *) Led_OnBoard__BIT_MASK)
/* Bypass Enable */
#define Led_OnBoard_BYP                    (* (reg8 *) Led_OnBoard__BYP)
/* Port wide control signals */                                                   
#define Led_OnBoard_CTL                    (* (reg8 *) Led_OnBoard__CTL)
/* Drive Modes */
#define Led_OnBoard_DM0                    (* (reg8 *) Led_OnBoard__DM0) 
#define Led_OnBoard_DM1                    (* (reg8 *) Led_OnBoard__DM1)
#define Led_OnBoard_DM2                    (* (reg8 *) Led_OnBoard__DM2) 
/* Input Buffer Disable Override */
#define Led_OnBoard_INP_DIS                (* (reg8 *) Led_OnBoard__INP_DIS)
/* LCD Common or Segment Drive */
#define Led_OnBoard_LCD_COM_SEG            (* (reg8 *) Led_OnBoard__LCD_COM_SEG)
/* Enable Segment LCD */
#define Led_OnBoard_LCD_EN                 (* (reg8 *) Led_OnBoard__LCD_EN)
/* Slew Rate Control */
#define Led_OnBoard_SLW                    (* (reg8 *) Led_OnBoard__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Led_OnBoard_PRTDSI__CAPS_SEL       (* (reg8 *) Led_OnBoard__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Led_OnBoard_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Led_OnBoard__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Led_OnBoard_PRTDSI__OE_SEL0        (* (reg8 *) Led_OnBoard__PRTDSI__OE_SEL0) 
#define Led_OnBoard_PRTDSI__OE_SEL1        (* (reg8 *) Led_OnBoard__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Led_OnBoard_PRTDSI__OUT_SEL0       (* (reg8 *) Led_OnBoard__PRTDSI__OUT_SEL0) 
#define Led_OnBoard_PRTDSI__OUT_SEL1       (* (reg8 *) Led_OnBoard__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Led_OnBoard_PRTDSI__SYNC_OUT       (* (reg8 *) Led_OnBoard__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Led_OnBoard__SIO_CFG)
    #define Led_OnBoard_SIO_HYST_EN        (* (reg8 *) Led_OnBoard__SIO_HYST_EN)
    #define Led_OnBoard_SIO_REG_HIFREQ     (* (reg8 *) Led_OnBoard__SIO_REG_HIFREQ)
    #define Led_OnBoard_SIO_CFG            (* (reg8 *) Led_OnBoard__SIO_CFG)
    #define Led_OnBoard_SIO_DIFF           (* (reg8 *) Led_OnBoard__SIO_DIFF)
#endif /* (Led_OnBoard__SIO_CFG) */

/* Interrupt Registers */
#if defined(Led_OnBoard__INTSTAT)
    #define Led_OnBoard_INTSTAT            (* (reg8 *) Led_OnBoard__INTSTAT)
    #define Led_OnBoard_SNAP               (* (reg8 *) Led_OnBoard__SNAP)
    
	#define Led_OnBoard_0_INTTYPE_REG 		(* (reg8 *) Led_OnBoard__0__INTTYPE)
#endif /* (Led_OnBoard__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Led_OnBoard_H */


/* [] END OF FILE */
