/*******************************************************************************
* File Name: PC.h
* Version 1.50
*
* Description:
*  This file provides constants and parameter values for the Software Transmit
*  UART Component.
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#ifndef CY_SW_TX_UART_PC_H
#define CY_SW_TX_UART_PC_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"

#define PC_BAUD_RATE                      (115200u)
#define PC_PIN_STATIC_MODE                (1u)


/***************************************
*        Function Prototypes
***************************************/
#if(PC_PIN_STATIC_MODE == 1u)
    void PC_Start(void) ;
#else
    void PC_StartEx(uint8 port, uint8 pin) ;
#endif /* (PC_PIN_STATIC_MODE == 1u) */

void PC_Stop(void) ;
void PC_PutChar(uint8 txDataByte) ;
void PC_PutString(const char8 string[]) ;
void PC_PutArray(const uint8 array[], uint32 byteCount) ;
void PC_PutHexByte(uint8 txHexByte) ;
void PC_PutHexInt(uint16 txHexInt) ;
void PC_PutCRLF(void) ;

#endif /* CY_SW_TX_UART_PC_H */


/* [] END OF FILE */
