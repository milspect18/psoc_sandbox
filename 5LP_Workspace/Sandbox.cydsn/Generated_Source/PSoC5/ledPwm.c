/*******************************************************************************
* File Name: ledPwm.c
* Version 3.30
*
* Description:
*  The PWM User Module consist of an 8 or 16-bit counter with two 8 or 16-bit
*  comparitors. Each instance of this user module is capable of generating
*  two PWM outputs with the same period. The pulse width is selectable between
*  1 and 255/65535. The period is selectable between 2 and 255/65536 clocks.
*  The compare value output may be configured to be active when the present
*  counter is less than or less than/equal to the compare value.
*  A terminal count output is also provided. It generates a pulse one clock
*  width wide when the counter is equal to zero.
*
* Note:
*
*******************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "ledPwm.h"

/* Error message for removed <resource> through optimization */
#ifdef ledPwm_PWMUDB_genblk1_ctrlreg__REMOVED
    #error PWM_v3_30 detected with a constant 0 for the enable or \
         constant 1 for reset. This will prevent the component from operating.
#endif /* ledPwm_PWMUDB_genblk1_ctrlreg__REMOVED */

uint8 ledPwm_initVar = 0u;


/*******************************************************************************
* Function Name: ledPwm_Start
********************************************************************************
*
* Summary:
*  The start function initializes the pwm with the default values, the
*  enables the counter to begin counting.  It does not enable interrupts,
*  the EnableInt command should be called if interrupt generation is required.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  ledPwm_initVar: Is modified when this function is called for the
*   first time. Is used to ensure that initialization happens only once.
*
*******************************************************************************/
void ledPwm_Start(void) 
{
    /* If not Initialized then initialize all required hardware and software */
    if(ledPwm_initVar == 0u)
    {
        ledPwm_Init();
        ledPwm_initVar = 1u;
    }
    ledPwm_Enable();

}


/*******************************************************************************
* Function Name: ledPwm_Init
********************************************************************************
*
* Summary:
*  Initialize component's parameters to the parameters set by user in the
*  customizer of the component placed onto schematic. Usually called in
*  ledPwm_Start().
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ledPwm_Init(void) 
{
    #if (ledPwm_UsingFixedFunction || ledPwm_UseControl)
        uint8 ctrl;
    #endif /* (ledPwm_UsingFixedFunction || ledPwm_UseControl) */

    #if(!ledPwm_UsingFixedFunction)
        #if(ledPwm_UseStatus)
            /* Interrupt State Backup for Critical Region*/
            uint8 ledPwm_interruptState;
        #endif /* (ledPwm_UseStatus) */
    #endif /* (!ledPwm_UsingFixedFunction) */

    #if (ledPwm_UsingFixedFunction)
        /* You are allowed to write the compare value (FF only) */
        ledPwm_CONTROL |= ledPwm_CFG0_MODE;
        #if (ledPwm_DeadBand2_4)
            ledPwm_CONTROL |= ledPwm_CFG0_DB;
        #endif /* (ledPwm_DeadBand2_4) */

        ctrl = ledPwm_CONTROL3 & ((uint8 )(~ledPwm_CTRL_CMPMODE1_MASK));
        ledPwm_CONTROL3 = ctrl | ledPwm_DEFAULT_COMPARE1_MODE;

         /* Clear and Set SYNCTC and SYNCCMP bits of RT1 register */
        ledPwm_RT1 &= ((uint8)(~ledPwm_RT1_MASK));
        ledPwm_RT1 |= ledPwm_SYNC;

        /*Enable DSI Sync all all inputs of the PWM*/
        ledPwm_RT1 &= ((uint8)(~ledPwm_SYNCDSI_MASK));
        ledPwm_RT1 |= ledPwm_SYNCDSI_EN;

    #elif (ledPwm_UseControl)
        /* Set the default compare mode defined in the parameter */
        ctrl = ledPwm_CONTROL & ((uint8)(~ledPwm_CTRL_CMPMODE2_MASK)) &
                ((uint8)(~ledPwm_CTRL_CMPMODE1_MASK));
        ledPwm_CONTROL = ctrl | ledPwm_DEFAULT_COMPARE2_MODE |
                                   ledPwm_DEFAULT_COMPARE1_MODE;
    #endif /* (ledPwm_UsingFixedFunction) */

    #if (!ledPwm_UsingFixedFunction)
        #if (ledPwm_Resolution == 8)
            /* Set FIFO 0 to 1 byte register for period*/
            ledPwm_AUX_CONTROLDP0 |= (ledPwm_AUX_CTRL_FIFO0_CLR);
        #else /* (ledPwm_Resolution == 16)*/
            /* Set FIFO 0 to 1 byte register for period */
            ledPwm_AUX_CONTROLDP0 |= (ledPwm_AUX_CTRL_FIFO0_CLR);
            ledPwm_AUX_CONTROLDP1 |= (ledPwm_AUX_CTRL_FIFO0_CLR);
        #endif /* (ledPwm_Resolution == 8) */

        ledPwm_WriteCounter(ledPwm_INIT_PERIOD_VALUE);
    #endif /* (!ledPwm_UsingFixedFunction) */

    ledPwm_WritePeriod(ledPwm_INIT_PERIOD_VALUE);

        #if (ledPwm_UseOneCompareMode)
            ledPwm_WriteCompare(ledPwm_INIT_COMPARE_VALUE1);
        #else
            ledPwm_WriteCompare1(ledPwm_INIT_COMPARE_VALUE1);
            ledPwm_WriteCompare2(ledPwm_INIT_COMPARE_VALUE2);
        #endif /* (ledPwm_UseOneCompareMode) */

        #if (ledPwm_KillModeMinTime)
            ledPwm_WriteKillTime(ledPwm_MinimumKillTime);
        #endif /* (ledPwm_KillModeMinTime) */

        #if (ledPwm_DeadBandUsed)
            ledPwm_WriteDeadTime(ledPwm_INIT_DEAD_TIME);
        #endif /* (ledPwm_DeadBandUsed) */

    #if (ledPwm_UseStatus || ledPwm_UsingFixedFunction)
        ledPwm_SetInterruptMode(ledPwm_INIT_INTERRUPTS_MODE);
    #endif /* (ledPwm_UseStatus || ledPwm_UsingFixedFunction) */

    #if (ledPwm_UsingFixedFunction)
        /* Globally Enable the Fixed Function Block chosen */
        ledPwm_GLOBAL_ENABLE |= ledPwm_BLOCK_EN_MASK;
        /* Set the Interrupt source to come from the status register */
        ledPwm_CONTROL2 |= ledPwm_CTRL2_IRQ_SEL;
    #else
        #if(ledPwm_UseStatus)

            /* CyEnterCriticalRegion and CyExitCriticalRegion are used to mark following region critical*/
            /* Enter Critical Region*/
            ledPwm_interruptState = CyEnterCriticalSection();
            /* Use the interrupt output of the status register for IRQ output */
            ledPwm_STATUS_AUX_CTRL |= ledPwm_STATUS_ACTL_INT_EN_MASK;

             /* Exit Critical Region*/
            CyExitCriticalSection(ledPwm_interruptState);

            /* Clear the FIFO to enable the ledPwm_STATUS_FIFOFULL
                   bit to be set on FIFO full. */
            ledPwm_ClearFIFO();
        #endif /* (ledPwm_UseStatus) */
    #endif /* (ledPwm_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: ledPwm_Enable
********************************************************************************
*
* Summary:
*  Enables the PWM block operation
*
* Parameters:
*  None
*
* Return:
*  None
*
* Side Effects:
*  This works only if software enable mode is chosen
*
*******************************************************************************/
void ledPwm_Enable(void) 
{
    /* Globally Enable the Fixed Function Block chosen */
    #if (ledPwm_UsingFixedFunction)
        ledPwm_GLOBAL_ENABLE |= ledPwm_BLOCK_EN_MASK;
        ledPwm_GLOBAL_STBY_ENABLE |= ledPwm_BLOCK_STBY_EN_MASK;
    #endif /* (ledPwm_UsingFixedFunction) */

    /* Enable the PWM from the control register  */
    #if (ledPwm_UseControl || ledPwm_UsingFixedFunction)
        ledPwm_CONTROL |= ledPwm_CTRL_ENABLE;
    #endif /* (ledPwm_UseControl || ledPwm_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: ledPwm_Stop
********************************************************************************
*
* Summary:
*  The stop function halts the PWM, but does not change any modes or disable
*  interrupts.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Side Effects:
*  If the Enable mode is set to Hardware only then this function
*  has no effect on the operation of the PWM
*
*******************************************************************************/
void ledPwm_Stop(void) 
{
    #if (ledPwm_UseControl || ledPwm_UsingFixedFunction)
        ledPwm_CONTROL &= ((uint8)(~ledPwm_CTRL_ENABLE));
    #endif /* (ledPwm_UseControl || ledPwm_UsingFixedFunction) */

    /* Globally disable the Fixed Function Block chosen */
    #if (ledPwm_UsingFixedFunction)
        ledPwm_GLOBAL_ENABLE &= ((uint8)(~ledPwm_BLOCK_EN_MASK));
        ledPwm_GLOBAL_STBY_ENABLE &= ((uint8)(~ledPwm_BLOCK_STBY_EN_MASK));
    #endif /* (ledPwm_UsingFixedFunction) */
}

#if (ledPwm_UseOneCompareMode)
    #if (ledPwm_CompareMode1SW)


        /*******************************************************************************
        * Function Name: ledPwm_SetCompareMode
        ********************************************************************************
        *
        * Summary:
        *  This function writes the Compare Mode for the pwm output when in Dither mode,
        *  Center Align Mode or One Output Mode.
        *
        * Parameters:
        *  comparemode:  The new compare mode for the PWM output. Use the compare types
        *                defined in the H file as input arguments.
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void ledPwm_SetCompareMode(uint8 comparemode) 
        {
            #if(ledPwm_UsingFixedFunction)

                #if(0 != ledPwm_CTRL_CMPMODE1_SHIFT)
                    uint8 comparemodemasked = ((uint8)((uint8)comparemode << ledPwm_CTRL_CMPMODE1_SHIFT));
                #else
                    uint8 comparemodemasked = comparemode;
                #endif /* (0 != ledPwm_CTRL_CMPMODE1_SHIFT) */

                ledPwm_CONTROL3 &= ((uint8)(~ledPwm_CTRL_CMPMODE1_MASK)); /*Clear Existing Data */
                ledPwm_CONTROL3 |= comparemodemasked;

            #elif (ledPwm_UseControl)

                #if(0 != ledPwm_CTRL_CMPMODE1_SHIFT)
                    uint8 comparemode1masked = ((uint8)((uint8)comparemode << ledPwm_CTRL_CMPMODE1_SHIFT)) &
                                                ledPwm_CTRL_CMPMODE1_MASK;
                #else
                    uint8 comparemode1masked = comparemode & ledPwm_CTRL_CMPMODE1_MASK;
                #endif /* (0 != ledPwm_CTRL_CMPMODE1_SHIFT) */

                #if(0 != ledPwm_CTRL_CMPMODE2_SHIFT)
                    uint8 comparemode2masked = ((uint8)((uint8)comparemode << ledPwm_CTRL_CMPMODE2_SHIFT)) &
                                               ledPwm_CTRL_CMPMODE2_MASK;
                #else
                    uint8 comparemode2masked = comparemode & ledPwm_CTRL_CMPMODE2_MASK;
                #endif /* (0 != ledPwm_CTRL_CMPMODE2_SHIFT) */

                /*Clear existing mode */
                ledPwm_CONTROL &= ((uint8)(~(ledPwm_CTRL_CMPMODE1_MASK |
                                            ledPwm_CTRL_CMPMODE2_MASK)));
                ledPwm_CONTROL |= (comparemode1masked | comparemode2masked);

            #else
                uint8 temp = comparemode;
            #endif /* (ledPwm_UsingFixedFunction) */
        }
    #endif /* ledPwm_CompareMode1SW */

#else /* UseOneCompareMode */

    #if (ledPwm_CompareMode1SW)


        /*******************************************************************************
        * Function Name: ledPwm_SetCompareMode1
        ********************************************************************************
        *
        * Summary:
        *  This function writes the Compare Mode for the pwm or pwm1 output
        *
        * Parameters:
        *  comparemode:  The new compare mode for the PWM output. Use the compare types
        *                defined in the H file as input arguments.
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void ledPwm_SetCompareMode1(uint8 comparemode) 
        {
            #if(0 != ledPwm_CTRL_CMPMODE1_SHIFT)
                uint8 comparemodemasked = ((uint8)((uint8)comparemode << ledPwm_CTRL_CMPMODE1_SHIFT)) &
                                           ledPwm_CTRL_CMPMODE1_MASK;
            #else
                uint8 comparemodemasked = comparemode & ledPwm_CTRL_CMPMODE1_MASK;
            #endif /* (0 != ledPwm_CTRL_CMPMODE1_SHIFT) */

            #if (ledPwm_UseControl)
                ledPwm_CONTROL &= ((uint8)(~ledPwm_CTRL_CMPMODE1_MASK)); /*Clear existing mode */
                ledPwm_CONTROL |= comparemodemasked;
            #endif /* (ledPwm_UseControl) */
        }
    #endif /* ledPwm_CompareMode1SW */

#if (ledPwm_CompareMode2SW)


    /*******************************************************************************
    * Function Name: ledPwm_SetCompareMode2
    ********************************************************************************
    *
    * Summary:
    *  This function writes the Compare Mode for the pwm or pwm2 output
    *
    * Parameters:
    *  comparemode:  The new compare mode for the PWM output. Use the compare types
    *                defined in the H file as input arguments.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void ledPwm_SetCompareMode2(uint8 comparemode) 
    {

        #if(0 != ledPwm_CTRL_CMPMODE2_SHIFT)
            uint8 comparemodemasked = ((uint8)((uint8)comparemode << ledPwm_CTRL_CMPMODE2_SHIFT)) &
                                                 ledPwm_CTRL_CMPMODE2_MASK;
        #else
            uint8 comparemodemasked = comparemode & ledPwm_CTRL_CMPMODE2_MASK;
        #endif /* (0 != ledPwm_CTRL_CMPMODE2_SHIFT) */

        #if (ledPwm_UseControl)
            ledPwm_CONTROL &= ((uint8)(~ledPwm_CTRL_CMPMODE2_MASK)); /*Clear existing mode */
            ledPwm_CONTROL |= comparemodemasked;
        #endif /* (ledPwm_UseControl) */
    }
    #endif /*ledPwm_CompareMode2SW */

#endif /* UseOneCompareMode */


#if (!ledPwm_UsingFixedFunction)


    /*******************************************************************************
    * Function Name: ledPwm_WriteCounter
    ********************************************************************************
    *
    * Summary:
    *  Writes a new counter value directly to the counter register. This will be
    *  implemented for that currently running period and only that period. This API
    *  is valid only for UDB implementation and not available for fixed function
    *  PWM implementation.
    *
    * Parameters:
    *  counter:  The period new period counter value.
    *
    * Return:
    *  None
    *
    * Side Effects:
    *  The PWM Period will be reloaded when a counter value will be a zero
    *
    *******************************************************************************/
    void ledPwm_WriteCounter(uint8 counter) \
                                       
    {
        CY_SET_REG8(ledPwm_COUNTER_LSB_PTR, counter);
    }


    /*******************************************************************************
    * Function Name: ledPwm_ReadCounter
    ********************************************************************************
    *
    * Summary:
    *  This function returns the current value of the counter.  It doesn't matter
    *  if the counter is enabled or running.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  The current value of the counter.
    *
    *******************************************************************************/
    uint8 ledPwm_ReadCounter(void) 
    {
        /* Force capture by reading Accumulator */
        /* Must first do a software capture to be able to read the counter */
        /* It is up to the user code to make sure there isn't already captured data in the FIFO */
          (void)CY_GET_REG8(ledPwm_COUNTERCAP_LSB_PTR_8BIT);

        /* Read the data from the FIFO */
        return (CY_GET_REG8(ledPwm_CAPTURE_LSB_PTR));
    }

    #if (ledPwm_UseStatus)


        /*******************************************************************************
        * Function Name: ledPwm_ClearFIFO
        ********************************************************************************
        *
        * Summary:
        *  This function clears all capture data from the capture FIFO
        *
        * Parameters:
        *  None
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void ledPwm_ClearFIFO(void) 
        {
            while(0u != (ledPwm_ReadStatusRegister() & ledPwm_STATUS_FIFONEMPTY))
            {
                (void)ledPwm_ReadCapture();
            }
        }

    #endif /* ledPwm_UseStatus */

#endif /* !ledPwm_UsingFixedFunction */


/*******************************************************************************
* Function Name: ledPwm_WritePeriod
********************************************************************************
*
* Summary:
*  This function is used to change the period of the counter.  The new period
*  will be loaded the next time terminal count is detected.
*
* Parameters:
*  period:  Period value. May be between 1 and (2^Resolution)-1.  A value of 0
*           will result in the counter remaining at zero.
*
* Return:
*  None
*
*******************************************************************************/
void ledPwm_WritePeriod(uint8 period) 
{
    #if(ledPwm_UsingFixedFunction)
        CY_SET_REG16(ledPwm_PERIOD_LSB_PTR, (uint16)period);
    #else
        CY_SET_REG8(ledPwm_PERIOD_LSB_PTR, period);
    #endif /* (ledPwm_UsingFixedFunction) */
}

#if (ledPwm_UseOneCompareMode)


    /*******************************************************************************
    * Function Name: ledPwm_WriteCompare
    ********************************************************************************
    *
    * Summary:
    *  This funtion is used to change the compare1 value when the PWM is in Dither
    *  mode. The compare output will reflect the new value on the next UDB clock.
    *  The compare output will be driven high when the present counter value is
    *  compared to the compare value based on the compare mode defined in
    *  Dither Mode.
    *
    * Parameters:
    *  compare:  New compare value.
    *
    * Return:
    *  None
    *
    * Side Effects:
    *  This function is only available if the PWM mode parameter is set to
    *  Dither Mode, Center Aligned Mode or One Output Mode
    *
    *******************************************************************************/
    void ledPwm_WriteCompare(uint8 compare) \
                                       
    {
        #if(ledPwm_UsingFixedFunction)
            CY_SET_REG16(ledPwm_COMPARE1_LSB_PTR, (uint16)compare);
        #else
            CY_SET_REG8(ledPwm_COMPARE1_LSB_PTR, compare);
        #endif /* (ledPwm_UsingFixedFunction) */

        #if (ledPwm_PWMMode == ledPwm__B_PWM__DITHER)
            #if(ledPwm_UsingFixedFunction)
                CY_SET_REG16(ledPwm_COMPARE2_LSB_PTR, (uint16)(compare + 1u));
            #else
                CY_SET_REG8(ledPwm_COMPARE2_LSB_PTR, (compare + 1u));
            #endif /* (ledPwm_UsingFixedFunction) */
        #endif /* (ledPwm_PWMMode == ledPwm__B_PWM__DITHER) */
    }


#else


    /*******************************************************************************
    * Function Name: ledPwm_WriteCompare1
    ********************************************************************************
    *
    * Summary:
    *  This funtion is used to change the compare1 value.  The compare output will
    *  reflect the new value on the next UDB clock.  The compare output will be
    *  driven high when the present counter value is less than or less than or
    *  equal to the compare register, depending on the mode.
    *
    * Parameters:
    *  compare:  New compare value.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void ledPwm_WriteCompare1(uint8 compare) \
                                        
    {
        #if(ledPwm_UsingFixedFunction)
            CY_SET_REG16(ledPwm_COMPARE1_LSB_PTR, (uint16)compare);
        #else
            CY_SET_REG8(ledPwm_COMPARE1_LSB_PTR, compare);
        #endif /* (ledPwm_UsingFixedFunction) */
    }


    /*******************************************************************************
    * Function Name: ledPwm_WriteCompare2
    ********************************************************************************
    *
    * Summary:
    *  This funtion is used to change the compare value, for compare1 output.
    *  The compare output will reflect the new value on the next UDB clock.
    *  The compare output will be driven high when the present counter value is
    *  less than or less than or equal to the compare register, depending on the
    *  mode.
    *
    * Parameters:
    *  compare:  New compare value.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void ledPwm_WriteCompare2(uint8 compare) \
                                        
    {
        #if(ledPwm_UsingFixedFunction)
            CY_SET_REG16(ledPwm_COMPARE2_LSB_PTR, compare);
        #else
            CY_SET_REG8(ledPwm_COMPARE2_LSB_PTR, compare);
        #endif /* (ledPwm_UsingFixedFunction) */
    }
#endif /* UseOneCompareMode */

#if (ledPwm_DeadBandUsed)


    /*******************************************************************************
    * Function Name: ledPwm_WriteDeadTime
    ********************************************************************************
    *
    * Summary:
    *  This function writes the dead-band counts to the corresponding register
    *
    * Parameters:
    *  deadtime:  Number of counts for dead time
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void ledPwm_WriteDeadTime(uint8 deadtime) 
    {
        /* If using the Dead Band 1-255 mode then just write the register */
        #if(!ledPwm_DeadBand2_4)
            CY_SET_REG8(ledPwm_DEADBAND_COUNT_PTR, deadtime);
        #else
            /* Otherwise the data has to be masked and offset */
            /* Clear existing data */
            ledPwm_DEADBAND_COUNT &= ((uint8)(~ledPwm_DEADBAND_COUNT_MASK));

            /* Set new dead time */
            #if(ledPwm_DEADBAND_COUNT_SHIFT)
                ledPwm_DEADBAND_COUNT |= ((uint8)((uint8)deadtime << ledPwm_DEADBAND_COUNT_SHIFT)) &
                                                    ledPwm_DEADBAND_COUNT_MASK;
            #else
                ledPwm_DEADBAND_COUNT |= deadtime & ledPwm_DEADBAND_COUNT_MASK;
            #endif /* (ledPwm_DEADBAND_COUNT_SHIFT) */

        #endif /* (!ledPwm_DeadBand2_4) */
    }


    /*******************************************************************************
    * Function Name: ledPwm_ReadDeadTime
    ********************************************************************************
    *
    * Summary:
    *  This function reads the dead-band counts from the corresponding register
    *
    * Parameters:
    *  None
    *
    * Return:
    *  Dead Band Counts
    *
    *******************************************************************************/
    uint8 ledPwm_ReadDeadTime(void) 
    {
        /* If using the Dead Band 1-255 mode then just read the register */
        #if(!ledPwm_DeadBand2_4)
            return (CY_GET_REG8(ledPwm_DEADBAND_COUNT_PTR));
        #else

            /* Otherwise the data has to be masked and offset */
            #if(ledPwm_DEADBAND_COUNT_SHIFT)
                return ((uint8)(((uint8)(ledPwm_DEADBAND_COUNT & ledPwm_DEADBAND_COUNT_MASK)) >>
                                                                           ledPwm_DEADBAND_COUNT_SHIFT));
            #else
                return (ledPwm_DEADBAND_COUNT & ledPwm_DEADBAND_COUNT_MASK);
            #endif /* (ledPwm_DEADBAND_COUNT_SHIFT) */
        #endif /* (!ledPwm_DeadBand2_4) */
    }
#endif /* DeadBandUsed */

#if (ledPwm_UseStatus || ledPwm_UsingFixedFunction)


    /*******************************************************************************
    * Function Name: ledPwm_SetInterruptMode
    ********************************************************************************
    *
    * Summary:
    *  This function configures the interrupts mask control of theinterrupt
    *  source status register.
    *
    * Parameters:
    *  uint8 interruptMode: Bit field containing the interrupt sources enabled
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void ledPwm_SetInterruptMode(uint8 interruptMode) 
    {
        CY_SET_REG8(ledPwm_STATUS_MASK_PTR, interruptMode);
    }


    /*******************************************************************************
    * Function Name: ledPwm_ReadStatusRegister
    ********************************************************************************
    *
    * Summary:
    *  This function returns the current state of the status register.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8 : Current status register value. The status register bits are:
    *  [7:6] : Unused(0)
    *  [5]   : Kill event output
    *  [4]   : FIFO not empty
    *  [3]   : FIFO full
    *  [2]   : Terminal count
    *  [1]   : Compare output 2
    *  [0]   : Compare output 1
    *
    *******************************************************************************/
    uint8 ledPwm_ReadStatusRegister(void) 
    {
        return (CY_GET_REG8(ledPwm_STATUS_PTR));
    }

#endif /* (ledPwm_UseStatus || ledPwm_UsingFixedFunction) */


#if (ledPwm_UseControl)


    /*******************************************************************************
    * Function Name: ledPwm_ReadControlRegister
    ********************************************************************************
    *
    * Summary:
    *  Returns the current state of the control register. This API is available
    *  only if the control register is not removed.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8 : Current control register value
    *
    *******************************************************************************/
    uint8 ledPwm_ReadControlRegister(void) 
    {
        uint8 result;

        result = CY_GET_REG8(ledPwm_CONTROL_PTR);
        return (result);
    }


    /*******************************************************************************
    * Function Name: ledPwm_WriteControlRegister
    ********************************************************************************
    *
    * Summary:
    *  Sets the bit field of the control register. This API is available only if
    *  the control register is not removed.
    *
    * Parameters:
    *  uint8 control: Control register bit field, The status register bits are:
    *  [7]   : PWM Enable
    *  [6]   : Reset
    *  [5:3] : Compare Mode2
    *  [2:0] : Compare Mode2
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void ledPwm_WriteControlRegister(uint8 control) 
    {
        CY_SET_REG8(ledPwm_CONTROL_PTR, control);
    }

#endif /* (ledPwm_UseControl) */


#if (!ledPwm_UsingFixedFunction)


    /*******************************************************************************
    * Function Name: ledPwm_ReadCapture
    ********************************************************************************
    *
    * Summary:
    *  Reads the capture value from the capture FIFO.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8/uint16: The current capture value
    *
    *******************************************************************************/
    uint8 ledPwm_ReadCapture(void) 
    {
        return (CY_GET_REG8(ledPwm_CAPTURE_LSB_PTR));
    }

#endif /* (!ledPwm_UsingFixedFunction) */


#if (ledPwm_UseOneCompareMode)


    /*******************************************************************************
    * Function Name: ledPwm_ReadCompare
    ********************************************************************************
    *
    * Summary:
    *  Reads the compare value for the compare output when the PWM Mode parameter is
    *  set to Dither mode, Center Aligned mode, or One Output mode.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8/uint16: Current compare value
    *
    *******************************************************************************/
    uint8 ledPwm_ReadCompare(void) 
    {
        #if(ledPwm_UsingFixedFunction)
            return ((uint8)CY_GET_REG16(ledPwm_COMPARE1_LSB_PTR));
        #else
            return (CY_GET_REG8(ledPwm_COMPARE1_LSB_PTR));
        #endif /* (ledPwm_UsingFixedFunction) */
    }

#else


    /*******************************************************************************
    * Function Name: ledPwm_ReadCompare1
    ********************************************************************************
    *
    * Summary:
    *  Reads the compare value for the compare1 output.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8/uint16: Current compare value.
    *
    *******************************************************************************/
    uint8 ledPwm_ReadCompare1(void) 
    {
        return (CY_GET_REG8(ledPwm_COMPARE1_LSB_PTR));
    }


    /*******************************************************************************
    * Function Name: ledPwm_ReadCompare2
    ********************************************************************************
    *
    * Summary:
    *  Reads the compare value for the compare2 output.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8/uint16: Current compare value.
    *
    *******************************************************************************/
    uint8 ledPwm_ReadCompare2(void) 
    {
        return (CY_GET_REG8(ledPwm_COMPARE2_LSB_PTR));
    }

#endif /* (ledPwm_UseOneCompareMode) */


/*******************************************************************************
* Function Name: ledPwm_ReadPeriod
********************************************************************************
*
* Summary:
*  Reads the period value used by the PWM hardware.
*
* Parameters:
*  None
*
* Return:
*  uint8/16: Period value
*
*******************************************************************************/
uint8 ledPwm_ReadPeriod(void) 
{
    #if(ledPwm_UsingFixedFunction)
        return ((uint8)CY_GET_REG16(ledPwm_PERIOD_LSB_PTR));
    #else
        return (CY_GET_REG8(ledPwm_PERIOD_LSB_PTR));
    #endif /* (ledPwm_UsingFixedFunction) */
}

#if ( ledPwm_KillModeMinTime)


    /*******************************************************************************
    * Function Name: ledPwm_WriteKillTime
    ********************************************************************************
    *
    * Summary:
    *  Writes the kill time value used by the hardware when the Kill Mode
    *  is set to Minimum Time.
    *
    * Parameters:
    *  uint8: Minimum Time kill counts
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void ledPwm_WriteKillTime(uint8 killtime) 
    {
        CY_SET_REG8(ledPwm_KILLMODEMINTIME_PTR, killtime);
    }


    /*******************************************************************************
    * Function Name: ledPwm_ReadKillTime
    ********************************************************************************
    *
    * Summary:
    *  Reads the kill time value used by the hardware when the Kill Mode is set
    *  to Minimum Time.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8: The current Minimum Time kill counts
    *
    *******************************************************************************/
    uint8 ledPwm_ReadKillTime(void) 
    {
        return (CY_GET_REG8(ledPwm_KILLMODEMINTIME_PTR));
    }

#endif /* ( ledPwm_KillModeMinTime) */

/* [] END OF FILE */
