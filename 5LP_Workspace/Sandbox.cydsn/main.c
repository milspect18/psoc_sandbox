/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

#define EVER ;;
#define LED_PWM_CP_MAX 99

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    ledPwm_Start();
    uint8_t i = 0;

    for(EVER)
    {
        /* Place your application code here. */
        while(i < (LED_PWM_CP_MAX - 1)) {
            ledPwm_WriteCompare(i++);
            CyDelay(10);
        }
        
        while(i > 1) {
            ledPwm_WriteCompare(i--);
            CyDelay(10);
        }
    }
}

/* [] END OF FILE */
