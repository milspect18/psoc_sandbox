/*******************************************************************************
* File Name: pwmClock.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_pwmClock_H)
#define CY_CLOCK_pwmClock_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component cy_clock_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5LP) */


/***************************************
*        Function Prototypes
***************************************/

void pwmClock_Start(void) ;
void pwmClock_Stop(void) ;

#if(CY_PSOC3 || CY_PSOC5LP)
void pwmClock_StopBlock(void) ;
#endif /* (CY_PSOC3 || CY_PSOC5LP) */

void pwmClock_StandbyPower(uint8 state) ;
void pwmClock_SetDividerRegister(uint16 clkDivider, uint8 restart) 
                                ;
uint16 pwmClock_GetDividerRegister(void) ;
void pwmClock_SetModeRegister(uint8 modeBitMask) ;
void pwmClock_ClearModeRegister(uint8 modeBitMask) ;
uint8 pwmClock_GetModeRegister(void) ;
void pwmClock_SetSourceRegister(uint8 clkSource) ;
uint8 pwmClock_GetSourceRegister(void) ;
#if defined(pwmClock__CFG3)
void pwmClock_SetPhaseRegister(uint8 clkPhase) ;
uint8 pwmClock_GetPhaseRegister(void) ;
#endif /* defined(pwmClock__CFG3) */

#define pwmClock_Enable()                       pwmClock_Start()
#define pwmClock_Disable()                      pwmClock_Stop()
#define pwmClock_SetDivider(clkDivider)         pwmClock_SetDividerRegister(clkDivider, 1u)
#define pwmClock_SetDividerValue(clkDivider)    pwmClock_SetDividerRegister((clkDivider) - 1u, 1u)
#define pwmClock_SetMode(clkMode)               pwmClock_SetModeRegister(clkMode)
#define pwmClock_SetSource(clkSource)           pwmClock_SetSourceRegister(clkSource)
#if defined(pwmClock__CFG3)
#define pwmClock_SetPhase(clkPhase)             pwmClock_SetPhaseRegister(clkPhase)
#define pwmClock_SetPhaseValue(clkPhase)        pwmClock_SetPhaseRegister((clkPhase) + 1u)
#endif /* defined(pwmClock__CFG3) */


/***************************************
*             Registers
***************************************/

/* Register to enable or disable the clock */
#define pwmClock_CLKEN              (* (reg8 *) pwmClock__PM_ACT_CFG)
#define pwmClock_CLKEN_PTR          ((reg8 *) pwmClock__PM_ACT_CFG)

/* Register to enable or disable the clock */
#define pwmClock_CLKSTBY            (* (reg8 *) pwmClock__PM_STBY_CFG)
#define pwmClock_CLKSTBY_PTR        ((reg8 *) pwmClock__PM_STBY_CFG)

/* Clock LSB divider configuration register. */
#define pwmClock_DIV_LSB            (* (reg8 *) pwmClock__CFG0)
#define pwmClock_DIV_LSB_PTR        ((reg8 *) pwmClock__CFG0)
#define pwmClock_DIV_PTR            ((reg16 *) pwmClock__CFG0)

/* Clock MSB divider configuration register. */
#define pwmClock_DIV_MSB            (* (reg8 *) pwmClock__CFG1)
#define pwmClock_DIV_MSB_PTR        ((reg8 *) pwmClock__CFG1)

/* Mode and source configuration register */
#define pwmClock_MOD_SRC            (* (reg8 *) pwmClock__CFG2)
#define pwmClock_MOD_SRC_PTR        ((reg8 *) pwmClock__CFG2)

#if defined(pwmClock__CFG3)
/* Analog clock phase configuration register */
#define pwmClock_PHASE              (* (reg8 *) pwmClock__CFG3)
#define pwmClock_PHASE_PTR          ((reg8 *) pwmClock__CFG3)
#endif /* defined(pwmClock__CFG3) */


/**************************************
*       Register Constants
**************************************/

/* Power manager register masks */
#define pwmClock_CLKEN_MASK         pwmClock__PM_ACT_MSK
#define pwmClock_CLKSTBY_MASK       pwmClock__PM_STBY_MSK

/* CFG2 field masks */
#define pwmClock_SRC_SEL_MSK        pwmClock__CFG2_SRC_SEL_MASK
#define pwmClock_MODE_MASK          (~(pwmClock_SRC_SEL_MSK))

#if defined(pwmClock__CFG3)
/* CFG3 phase mask */
#define pwmClock_PHASE_MASK         pwmClock__CFG3_PHASE_DLY_MASK
#endif /* defined(pwmClock__CFG3) */

#endif /* CY_CLOCK_pwmClock_H */


/* [] END OF FILE */
