/*******************************************************************************
* File Name: ledPwm_PM.c
* Version 3.30
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ledPwm.h"

static ledPwm_backupStruct ledPwm_backup;


/*******************************************************************************
* Function Name: ledPwm_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  ledPwm_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void ledPwm_SaveConfig(void) 
{

    #if(!ledPwm_UsingFixedFunction)
        #if(!ledPwm_PWMModeIsCenterAligned)
            ledPwm_backup.PWMPeriod = ledPwm_ReadPeriod();
        #endif /* (!ledPwm_PWMModeIsCenterAligned) */
        ledPwm_backup.PWMUdb = ledPwm_ReadCounter();
        #if (ledPwm_UseStatus)
            ledPwm_backup.InterruptMaskValue = ledPwm_STATUS_MASK;
        #endif /* (ledPwm_UseStatus) */

        #if(ledPwm_DeadBandMode == ledPwm__B_PWM__DBM_256_CLOCKS || \
            ledPwm_DeadBandMode == ledPwm__B_PWM__DBM_2_4_CLOCKS)
            ledPwm_backup.PWMdeadBandValue = ledPwm_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */

        #if(ledPwm_KillModeMinTime)
             ledPwm_backup.PWMKillCounterPeriod = ledPwm_ReadKillTime();
        #endif /* (ledPwm_KillModeMinTime) */

        #if(ledPwm_UseControl)
            ledPwm_backup.PWMControlRegister = ledPwm_ReadControlRegister();
        #endif /* (ledPwm_UseControl) */
    #endif  /* (!ledPwm_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: ledPwm_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  ledPwm_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void ledPwm_RestoreConfig(void) 
{
        #if(!ledPwm_UsingFixedFunction)
            #if(!ledPwm_PWMModeIsCenterAligned)
                ledPwm_WritePeriod(ledPwm_backup.PWMPeriod);
            #endif /* (!ledPwm_PWMModeIsCenterAligned) */

            ledPwm_WriteCounter(ledPwm_backup.PWMUdb);

            #if (ledPwm_UseStatus)
                ledPwm_STATUS_MASK = ledPwm_backup.InterruptMaskValue;
            #endif /* (ledPwm_UseStatus) */

            #if(ledPwm_DeadBandMode == ledPwm__B_PWM__DBM_256_CLOCKS || \
                ledPwm_DeadBandMode == ledPwm__B_PWM__DBM_2_4_CLOCKS)
                ledPwm_WriteDeadTime(ledPwm_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */

            #if(ledPwm_KillModeMinTime)
                ledPwm_WriteKillTime(ledPwm_backup.PWMKillCounterPeriod);
            #endif /* (ledPwm_KillModeMinTime) */

            #if(ledPwm_UseControl)
                ledPwm_WriteControlRegister(ledPwm_backup.PWMControlRegister);
            #endif /* (ledPwm_UseControl) */
        #endif  /* (!ledPwm_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: ledPwm_Sleep
********************************************************************************
*
* Summary:
*  Disables block's operation and saves the user configuration. Should be called
*  just prior to entering sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  ledPwm_backup.PWMEnableState:  Is modified depending on the enable
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void ledPwm_Sleep(void) 
{
    #if(ledPwm_UseControl)
        if(ledPwm_CTRL_ENABLE == (ledPwm_CONTROL & ledPwm_CTRL_ENABLE))
        {
            /*Component is enabled */
            ledPwm_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            ledPwm_backup.PWMEnableState = 0u;
        }
    #endif /* (ledPwm_UseControl) */

    /* Stop component */
    ledPwm_Stop();

    /* Save registers configuration */
    ledPwm_SaveConfig();
}


/*******************************************************************************
* Function Name: ledPwm_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration. Should be called just after
*  awaking from sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  ledPwm_backup.pwmEnable:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void ledPwm_Wakeup(void) 
{
     /* Restore registers values */
    ledPwm_RestoreConfig();

    if(ledPwm_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        ledPwm_Enable();
    } /* Do nothing if component's block was disabled before */

}


/* [] END OF FILE */
