EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:CY8C4245
LIBS:devBoard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CY8C4245AZI-483 U?
U 1 1 58F8DB34
P 4150 5425
F 0 "U?" H 4950 3525 60  0000 C CNN
F 1 "CY8C4245AZI-483" H 4150 3700 60  0000 C CNN
F 2 "Housings_QFP:TQFP-48_7x7mm_Pitch0.5mm" H 4175 5800 60  0001 C CNN
F 3 "" H 4175 5800 60  0000 C CNN
	1    4150 5425
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58F8DB3B
P 6025 4750
F 0 "#PWR?" H 6025 4500 50  0001 C CNN
F 1 "GND" H 6025 4600 50  0000 C CNN
F 2 "" H 6025 4750 50  0000 C CNN
F 3 "" H 6025 4750 50  0000 C CNN
	1    6025 4750
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 58F8DB41
P 6075 5200
F 0 "#PWR?" H 6075 4950 50  0001 C CNN
F 1 "GND" H 6075 5050 50  0000 C CNN
F 2 "" H 6075 5200 50  0000 C CNN
F 3 "" H 6075 5200 50  0000 C CNN
	1    6075 5200
	0    -1   -1   0   
$EndComp
Text GLabel 6025 4900 2    60   Input ~ 0
VDDA
Text GLabel 6025 5050 2    60   Input ~ 0
VDDD
Text GLabel 6050 5350 2    60   Input ~ 0
VCCD
$Comp
L GND #PWR?
U 1 1 58F8DB4A
P 2275 5800
F 0 "#PWR?" H 2275 5550 50  0001 C CNN
F 1 "GND" H 2275 5650 50  0000 C CNN
F 2 "" H 2275 5800 50  0000 C CNN
F 3 "" H 2275 5800 50  0000 C CNN
	1    2275 5800
	0    1    1    0   
$EndComp
Text GLabel 2275 5650 0    60   Input ~ 0
SWDIO
Text GLabel 2275 5950 0    60   Input ~ 0
SWDCLK
Text GLabel 2275 6700 0    60   Input ~ 0
VDDD
Text GLabel 2300 3850 0    60   Input ~ 0
Ping_1_Trig
Text GLabel 2300 4000 0    60   Input ~ 0
Ping_1_Echo
Text GLabel 2300 4150 0    60   Input ~ 0
Ping_2_Trig
Text GLabel 2300 4300 0    60   Input ~ 0
Ping_2_Echo
Text GLabel 2300 4450 0    60   Input ~ 0
Ping_3_Trig
Text GLabel 2300 4600 0    60   Input ~ 0
Ping_3_Echo
Text GLabel 2300 4750 0    60   Input ~ 0
Ping_4_Trig
Text GLabel 2300 4900 0    60   Input ~ 0
Ping_4_Echo
Text GLabel 2275 7000 0    60   Input ~ 0
I2C_SDA
Text GLabel 2275 6850 0    60   Input ~ 0
I2C_SCL
Wire Wire Line
	5300 4750 6025 4750
Wire Wire Line
	6075 5200 5300 5200
Wire Wire Line
	5300 4900 6025 4900
Wire Wire Line
	5300 5050 6025 5050
Wire Wire Line
	5300 5350 6050 5350
Wire Wire Line
	3000 5650 2275 5650
Wire Wire Line
	3000 5950 2275 5950
Wire Wire Line
	3000 5800 2275 5800
Wire Wire Line
	3000 6700 2275 6700
Wire Wire Line
	3000 3850 2300 3850
Wire Wire Line
	3000 4000 2300 4000
Wire Wire Line
	3000 4150 2300 4150
Wire Wire Line
	3000 4300 2300 4300
Wire Wire Line
	3000 4450 2300 4450
Wire Wire Line
	3000 4600 2300 4600
Wire Wire Line
	3000 4750 2300 4750
Wire Wire Line
	3000 4900 2300 4900
Wire Wire Line
	3000 7000 2275 7000
Wire Wire Line
	3000 6850 2275 6850
Wire Wire Line
	3000 5350 2300 5350
Text GLabel 2300 5350 0    60   Input ~ 0
READY
Text GLabel 6050 5500 2    60   Input ~ 0
RESET
Wire Wire Line
	6050 5500 5300 5500
Text GLabel 3000 3725 0    60   Input ~ 0
P1.7
Text GLabel 3000 5500 0    60   Input ~ 0
P3.1
Text GLabel 3000 6100 0    60   Input ~ 0
P3.4
Text GLabel 3000 6250 0    60   Input ~ 0
P3.5
Text GLabel 3000 6400 0    60   Input ~ 0
P3.6
Text GLabel 3000 6550 0    60   Input ~ 0
P3.7
Text GLabel 3000 7150 0    60   Input ~ 0
P4.2
Text GLabel 5300 7150 2    60   Input ~ 0
P4.3
Text GLabel 5300 6700 2    60   Input ~ 0
P0.0
Text GLabel 5300 6550 2    60   Input ~ 0
P0.1
Text GLabel 5300 6400 2    60   Input ~ 0
P0.2
Text GLabel 5300 6250 2    60   Input ~ 0
P0.3
Text GLabel 5300 6100 2    60   Input ~ 0
P0.4
Text GLabel 5300 5950 2    60   Input ~ 0
P0.5
Text GLabel 5300 5800 2    60   Input ~ 0
P0.6
Text GLabel 5300 5650 2    60   Input ~ 0
P0.7
Text GLabel 5300 4600 2    60   Input ~ 0
P1.0
Text GLabel 5300 4450 2    60   Input ~ 0
P1.1
Text GLabel 5300 4300 2    60   Input ~ 0
P1.2
Text GLabel 5300 4150 2    60   Input ~ 0
P1.3
Text GLabel 5300 4000 2    60   Input ~ 0
P1.4
Text GLabel 5300 3850 2    60   Input ~ 0
P1.5
Text GLabel 5300 3700 2    60   Input ~ 0
P1.6
Text GLabel 2675 1175 2    60   Input ~ 0
VDDA
Text GLabel 2675 1325 2    60   Input ~ 0
VDDD
Text GLabel 2675 1025 2    60   Input ~ 0
VDD
Wire Wire Line
	1975 1325 2675 1325
Wire Wire Line
	2575 1025 2575 1325
Wire Wire Line
	2575 1025 2675 1025
Wire Wire Line
	1175 1175 2675 1175
Connection ~ 2575 1175
Wire Wire Line
	1175 1175 1175 1275
Wire Wire Line
	1975 1325 1975 1425
Connection ~ 2575 1325
$Comp
L C_Small C?
U 1 1 58F8DED5
P 1175 1375
F 0 "C?" H 1185 1445 50  0000 L CNN
F 1 "0.1 uF" H 1185 1295 30  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1175 1375 50  0001 C CNN
F 3 "" H 1175 1375 50  0000 C CNN
	1    1175 1375
	1    0    0    -1  
$EndComp
$Comp
L C_Small C?
U 1 1 58F8DEDC
P 1500 1375
F 0 "C?" H 1510 1445 50  0000 L CNN
F 1 "1 uF" H 1510 1295 30  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1500 1375 50  0001 C CNN
F 3 "" H 1500 1375 50  0000 C CNN
	1    1500 1375
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58F8DEE3
P 1175 1575
F 0 "#PWR?" H 1175 1325 50  0001 C CNN
F 1 "GND" H 1175 1425 50  0000 C CNN
F 2 "" H 1175 1575 50  0000 C CNN
F 3 "" H 1175 1575 50  0000 C CNN
	1    1175 1575
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58F8DEE9
P 1500 1575
F 0 "#PWR?" H 1500 1325 50  0001 C CNN
F 1 "GND" H 1500 1425 50  0000 C CNN
F 2 "" H 1500 1575 50  0000 C CNN
F 3 "" H 1500 1575 50  0000 C CNN
	1    1500 1575
	1    0    0    -1  
$EndComp
Wire Wire Line
	1175 1475 1175 1575
Wire Wire Line
	1500 1475 1500 1575
Wire Wire Line
	1500 1175 1500 1275
Connection ~ 1500 1175
$Comp
L C_Small C?
U 1 1 58F8DEF3
P 1975 1525
F 0 "C?" H 1985 1595 50  0000 L CNN
F 1 "0.1 uF" H 1985 1445 30  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1975 1525 50  0001 C CNN
F 3 "" H 1975 1525 50  0000 C CNN
	1    1975 1525
	1    0    0    -1  
$EndComp
$Comp
L C_Small C?
U 1 1 58F8DEFA
P 2275 1525
F 0 "C?" H 2285 1595 50  0000 L CNN
F 1 "1 uF" H 2285 1445 30  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2275 1525 50  0001 C CNN
F 3 "" H 2275 1525 50  0000 C CNN
	1    2275 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	2275 1325 2275 1425
Connection ~ 2275 1325
$Comp
L GND #PWR?
U 1 1 58F8DF03
P 1975 1700
F 0 "#PWR?" H 1975 1450 50  0001 C CNN
F 1 "GND" H 1975 1550 50  0000 C CNN
F 2 "" H 1975 1700 50  0000 C CNN
F 3 "" H 1975 1700 50  0000 C CNN
	1    1975 1700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58F8DF09
P 2275 1700
F 0 "#PWR?" H 2275 1450 50  0001 C CNN
F 1 "GND" H 2275 1550 50  0000 C CNN
F 2 "" H 2275 1700 50  0000 C CNN
F 3 "" H 2275 1700 50  0000 C CNN
	1    2275 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1975 1625 1975 1700
Wire Wire Line
	2275 1625 2275 1700
Text GLabel 2675 2175 2    60   Input ~ 0
VCCD
$Comp
L C_Small C?
U 1 1 58F8DF12
P 2275 2375
F 0 "C?" H 2285 2445 50  0000 L CNN
F 1 "1 uF" H 2285 2295 30  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2275 2375 50  0001 C CNN
F 3 "" H 2275 2375 50  0000 C CNN
	1    2275 2375
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58F8DF19
P 2275 2575
F 0 "#PWR?" H 2275 2325 50  0001 C CNN
F 1 "GND" H 2275 2425 50  0000 C CNN
F 2 "" H 2275 2575 50  0000 C CNN
F 3 "" H 2275 2575 50  0000 C CNN
	1    2275 2575
	1    0    0    -1  
$EndComp
Wire Wire Line
	2275 2475 2275 2575
Wire Wire Line
	2675 2175 2275 2175
Wire Wire Line
	2275 2175 2275 2275
Wire Wire Line
	1175 2175 1875 2175
Wire Wire Line
	1175 2175 1175 2275
$Comp
L C_Small C?
U 1 1 58F8DF24
P 1175 2375
F 0 "C?" H 1185 2445 50  0000 L CNN
F 1 "0.1 uF" H 1185 2295 30  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1175 2375 50  0001 C CNN
F 3 "" H 1175 2375 50  0000 C CNN
	1    1175 2375
	1    0    0    -1  
$EndComp
$Comp
L C_Small C?
U 1 1 58F8DF2B
P 1475 2375
F 0 "C?" H 1485 2445 50  0000 L CNN
F 1 "1 uF" H 1485 2295 30  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1475 2375 50  0001 C CNN
F 3 "" H 1475 2375 50  0000 C CNN
	1    1475 2375
	1    0    0    -1  
$EndComp
Wire Wire Line
	1475 2175 1475 2275
Connection ~ 1475 2175
$Comp
L GND #PWR?
U 1 1 58F8DF34
P 1175 2550
F 0 "#PWR?" H 1175 2300 50  0001 C CNN
F 1 "GND" H 1175 2400 50  0000 C CNN
F 2 "" H 1175 2550 50  0000 C CNN
F 3 "" H 1175 2550 50  0000 C CNN
	1    1175 2550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58F8DF3A
P 1475 2550
F 0 "#PWR?" H 1475 2300 50  0001 C CNN
F 1 "GND" H 1475 2400 50  0000 C CNN
F 2 "" H 1475 2550 50  0000 C CNN
F 3 "" H 1475 2550 50  0000 C CNN
	1    1475 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1175 2475 1175 2550
Wire Wire Line
	1475 2475 1475 2550
Text GLabel 1875 2175 2    60   Input ~ 0
VDDD
Text Notes 1500 925  2    60   ~ 0
Filter / Bypass
Text Notes 2000 750  2    89   ~ 0
PSoC Input Power
$Comp
L CONN_01X05 P?
U 1 1 58F8E1C8
P 5975 1925
F 0 "P?" H 5975 2225 50  0000 C CNN
F 1 "PRG/DBG" V 6075 1925 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 5975 1925 50  0001 C CNN
F 3 "" H 5975 1925 50  0000 C CNN
	1    5975 1925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5775 1725 4925 1725
Text GLabel 4925 1725 0    60   Input ~ 0
VDD
$Comp
L GND #PWR?
U 1 1 58F8E1D1
P 5525 1825
F 0 "#PWR?" H 5525 1575 50  0001 C CNN
F 1 "GND" V 5525 1625 50  0000 C CNN
F 2 "" H 5525 1825 50  0000 C CNN
F 3 "" H 5525 1825 50  0000 C CNN
	1    5525 1825
	0    1    1    0   
$EndComp
Wire Wire Line
	5775 1825 5525 1825
Wire Wire Line
	5775 1925 4925 1925
Text GLabel 4925 1925 0    60   Input ~ 0
RESET
Wire Wire Line
	5775 2025 5425 2025
Wire Wire Line
	5775 2125 4925 2125
Text GLabel 5425 2025 0    60   Input ~ 0
SWDCLK
Text GLabel 4925 2125 0    60   Input ~ 0
SWDIO
Text Notes 5375 1525 2    89   ~ 0
KitProg Interface
$EndSCHEMATC
